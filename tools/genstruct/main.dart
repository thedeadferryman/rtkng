/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:io';

import 'package:args/args.dart';
import 'package:yaml/yaml.dart';

import 'gen_struct.dart';
import 'struct_def.dart';
import 'struct_io.dart';

ArgParser configureCLIOptions() => ArgParser()
  ..addOption(
    'rc',
    abbr: 'r',
    valueHelp: 'FILE',
    help: 'A configuration file to use',
    defaultsTo: './sgrc.yml',
  )
  ..addOption(
    'output',
    abbr: 'o',
    valueHelp: 'PATH',
    help: 'A directory to emit outputs to',
    defaultsTo: '.',
  )
  ..addMultiOption(
    'ignore',
    abbr: 'x',
    valueHelp: 'TARGET_NAME',
    help: 'Do not do anything for specified targets',
    defaultsTo: [],
  )
  ..addFlag(
    'emit',
    abbr: 'e',
    help:
        'When disabled, targets will not emit anything, but will be validated',
    defaultsTo: true,
  )
  ..addFlag(
    'help',
    abbr: 'h',
    aliases: ['usage', '?'],
    help: 'Show this help and exit',
    defaultsTo: false,
    negatable: false,
  );

class TargetModes {
  static const library = TargetModes.__('LIBRARY');
  static const header = TargetModes.__('HEADER');
  static const private = TargetModes.__('PRIVATE');

  static const modes = [library, header, private];

  final String id;

  const TargetModes.__(this.id);

  factory TargetModes.parse(String mode) {
    return modes.firstWhere((e) => e.id == mode, orElse: () {
      var mds = modes.map((e) => e.id).join(', ');
      throw ArgumentError.value(
          mode, 'mode', 'Invalid mode passed, expected [$mds]');
    });
  }
}

class Target {
  final TargetModes mode;
  final StructGeneratorContext context;
  final String basename;
  final String definitions;

  Target({
    required this.mode,
    required this.context,
    required this.basename,
    required this.definitions,
  });
}

class SGRC {
  final Map<String, Target> targets;

  SGRC(this.targets);
}

class SGRCDeserializer with DeserializerAssistance {
  final dynamic _data;

  SGRCDeserializer(String data) : _data = loadYaml(data);

  SGRCDeserializer.fromParsed(this._data);

  SGRC deserialize() {
    assertTyped<Map>(_data, name: '#root');

    var targets = assertTyped<Map>(_data['targets'], name: '#root.targets')
        .map((key, value) {
      assertTyped<Map>(value);

      var ctx = Map.fromEntries(
          (getTypedValue<Map>(value, 'context') ?? Map.identity()).entries);

      ctx['rootName'] = getTypedValue<String>(ctx, 'rootName') ?? key;

      var context = ContextDeserializer.fromParsed(ctx).deserialize();

      var target = Target(
        mode: TargetModes.parse(
            getTypedValue(value, 'mode') ?? TargetModes.library.id),
        context: context,
        basename: getTypedValue(value, 'fileBasename') ?? key,
        definitions: getTypedValue(value, 'definitions') ?? '$key.yml',
      );

      return MapEntry(assertTyped<String>(key), target);
    });

    return SGRC(targets);
  }
}

class AppExecutor {
  final File _configFile;
  final SGRC _rc;
  final ArgResults _args;

  AppExecutor.__(this._configFile, this._rc, this._args);

  static Future<AppExecutor> init(ArgResults args) async {
    var configFile = File(args['rc']);

    if (!await configFile.exists()) {
      throw ArgumentError.value(configFile.path, 'rc', 'File not found');
    }

    var rc = SGRCDeserializer(await configFile.readAsString()).deserialize();

    return AppExecutor.__(configFile, rc, args);
  }

  Future<Map<String, String>> processTarget(String key, Target target) async {
    if (_args['ignore'].contains(key)) return {};

    stdout.writeln('Processing target <$key>...');

    var defFile =
        File('${_configFile.absolute.parent.path}/${target.definitions}').absolute;

    if (!await defFile.exists()) {
      throw ArgumentError.value(
          defFile.path, '$key.definitions', 'File not found: ${defFile.path}');
    }

    var struct = await StructIO.readStruct(defFile);

    if (struct is! StructDefObject) {
      throw ArgumentError.value(struct, '$key:Struct',
          'Top-level definition of struct must be of type Object');
    }

    var sg = StructGenerator(target.context, struct);

    switch (target.mode) {
      case TargetModes.library:
        var header = sg.generate(outputType: StructOutputType.header);
        var source = sg.generate(
            headerFileName: '${target.basename}.h',
            outputType: StructOutputType.source);

        return {
          '${target.basename}.h': header,
          '${target.basename}.c': source,
        };
      case TargetModes.header:
        var header = sg.generate(outputType: StructOutputType.headerOnly);

        return {
          '${target.basename}.h': header,
        };
    }

    return {};
  }

  Future<void> execute() async {
    var targets = await Future.wait(_rc.targets.entries.map((entry) async {
      try {
        var mapEntry = MapEntry(
          entry.key,
          await processTarget(entry.key, entry.value),
        );

        stdout.writeln('Target ${entry.key} proccessed');

        return mapEntry;
      } on ArgumentError catch (e) {
        stderr.writeln('Target ${entry.key} failed: ${e.message}');
      }
    }));

    stdout.writeln();

    if (_args['emit']) {
      var outRoot = Directory(_args['output']).absolute;

      if (!await outRoot.exists()) {
        try {
          outRoot = await outRoot.create(recursive: true);
        } on FileSystemException catch (e) {
          stderr.writeln(
            "Unable to create output directory '${outRoot.path}': ",
          );
          stderr.writeln('  ${e.message}');

          exit(-1);
        }
      }

      await Future.forEach(targets,
          (MapEntry<String, Map<String, String>>? entry) async {
        if (entry == null) return;

        var key = entry.key;
        var emit = entry.value;

        stdout.writeln('Emitting output for target <$key>');

        await Future.forEach(emit.entries,
            (MapEntry<String, String> ent) async {
          var name = ent.key;
          var contents = ent.value;

          var path = File([outRoot.path, name].join('/')).absolute;

          stdout.writeln("  Writing '$name' to '${path.path}'");

          if (!await path.parent.exists()) {
            try {
              await path.parent.create(recursive: true);
            } on FileSystemException catch (e) {
              stderr.writeln("  Unable to write '$name': ");
              stderr.writeln(
                "    Unable to create directory: '${path.parent.path}'",
              );
              stderr.writeln('      ${e.message}');

              return;
            }
          }

          try {
            await path.writeAsString(contents, flush: true);
          } on FileSystemException catch (e) {
            stderr.writeln("  Unable to write '$name': ");
            stderr.writeln('    ${e.message}');

            return;
          }
        });
      });
    }
  }
}

Future<void> main(List<String> argv) async {
  var parser = configureCLIOptions();

  try {
    var args = parser.parse(argv);

    if (args['help']) {
      stdout.writeln('Usage: sg [OPTIONS]');
      stdout.writeln();
      stdout.writeln(parser.usage);
      return;
    }

    var executor = await AppExecutor.init(args);

    await executor.execute();
  } on ArgParserException catch (e) {
    stderr.writeln(e.message);
    stderr.writeln();
    stderr.writeln(parser.usage);
  }
}

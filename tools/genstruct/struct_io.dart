/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:yaml/yaml.dart';

import 'gen_struct.dart';
import 'struct_def.dart';

class StructIO {
  static Future<File> writeStruct(StructDef def, File dest) {
    var ser = StructSerializer(def);

    return dest.writeAsString(ser.serialize());
  }

  static Future<StructDef> readStruct(File src) async {
    var str = await src.readAsString();

    return StructDeserializer(str).deserialize();
  }
}

mixin DeserializerAssistance {
  T? getTypedValue<T>(Map<dynamic, dynamic> dict, String key,
      {bool notNull = false}) {
    var value = dict[key];

    if (value == null) {
      if (notNull) {
        throw ArgumentError.value(
            value, key, '<${T.runtimeType}> expected, got null');
      }

      return null;
    }

    return assertTyped(value, name: key);
  }

  T assertTyped<T>(dynamic value, {String name = 'value'}) {
    if (value is! T) {
      throw ArgumentError.value(
          value, name, '<${T.runtimeType}> expected, got null');
    }

    return value;
  }
}

class StructTypes {
  static const unit = StructTypes.__('Void');
  static const object = StructTypes.__('Object');
  static const list = StructTypes.__('List');
  static const integer = StructTypes.__('Integer');
  static const double = StructTypes.__('Double');
  static const string = StructTypes.__('String');

  static const intSignNames = ['signed', 'unsigned'];

  static const validTypes = [unit, object, list, integer, double, string];

  final String _name;

  String get name => _name;

  const StructTypes.__(this._name);

  static StructTypes parseType(String typeStr) {
    return validTypes.firstWhere((e) => e.name == typeStr, orElse: () => unit);
  }
}

class StructSerializer {
  final StructDef _def;

  StructSerializer(this._def);

  String serialize() {
    return json.encode(_serializeToMap(_def));
  }

  Map<String, dynamic> _serializeToMap(StructDef def) {
    var ser = HashMap<String, dynamic>.identity();

    ser['type'] = StructTypes.unit.name;

    if (def is StructDefObject) {
      ser['type'] = StructTypes.object.name;

      ser['fields'] =
          def.fields.map((key, value) => MapEntry(key, _serializeToMap(value)));
    }
    if (def is StructDefList) {
      ser['type'] = StructTypes.list.name;

      ser['items'] = _serializeToMap(def.item);
    }
    if (def is StructDefInteger) {
      ser['type'] = StructTypes.integer.name;

      ser['options'] = [StructTypes.intSignNames[def.sign ? 0 : 1], def.width];
    }
    if (def is StructDefDouble) {
      ser['type'] = StructTypes.double.name;
    }
    if (def is StructDefString) {
      ser['type'] = StructTypes.string.name;
    }

    return ser;
  }
}

class StructDeserializer {
  final dynamic _data;

  StructDeserializer(String data) : _data = loadYaml(data);

  StructDeserializer.fromParsed(this._data);

  StructDef deserialize() => _deserializeDeep(_data);

  StructDef _deserializeDeep(dynamic data, [String name = '#root']) {
    if (data is! Map) {
      throw ArgumentError.value(data, name, 'Dictionary expected');
    }

    var type = _parseType(data['type']);

    switch (type) {
      case StructTypes.object:
        return _parseObject(data, name);
      case StructTypes.list:
        return Struct.list(_deserializeDeep(data['items'], '$name.items'));
      case StructTypes.integer:
        return _parseInteger(data, name);
      case StructTypes.double:
        return Struct.double;
      case StructTypes.string:
        return Struct.string;
      case StructTypes.unit:
    }

    throw ArgumentError.value(data, name, 'Could not recognize type');
  }

  StructDefObject _parseObject(Map data, String name) {
    var obj = Struct.object();
    var fields = data['fields'];

    if (fields is! Map) {
      throw ArgumentError.value(
          fields, '$name.fields', 'No field definition for object');
    }

    fields.forEach((key, value) {
      obj.add(key, _deserializeDeep(value, '$name.fields.$key'));
    });

    return obj;
  }

  StructDefInteger _parseInteger(Map<dynamic, dynamic> data, String name) {
    var options = data['options'];

    if (options !is List) {
      throw ArgumentError.value(options, '$name.options', 'Array expected');
    }

    var signStr = options[0];
    var width = options[1];

    if (signStr is! String) {
      throw ArgumentError.value(signStr, '$name.options[0]', 'String expected');
    }
    if (width is! int) {
      throw ArgumentError.value(width, '$name.options[1]', 'Number expected');
    }

    var signInt = StructTypes.intSignNames.indexOf(signStr);

    if (signInt < 0) {
      throw ArgumentError.value(signStr, '$name.options[0]', 'Unknown value');
    }

    if (!StructDefInteger.supportedWidths.contains(width)) {
      throw ArgumentError.value(width, '$name.options[1]', 'Unsupported value');
    }

    return StructDefInteger(width, (signInt == 0 ? true : false));
  }

  StructTypes _parseType(dynamic type) {
    if (type is! String) return StructTypes.unit;

    return StructTypes.parseType(type);
  }
}

class ContextSerializer {
  final StructGeneratorContext _context;

  ContextSerializer(this._context);

  String serialize() {
    var plain = _plainifyContext(_context);
    var plainDef = _plainifyContext(defaultContext);

    plain.removeWhere((key, value) => value == plainDef[key]);

    return json.encode(plain);
  }

  static StructGeneratorContext get defaultContext =>
      StructGeneratorContext(rootName: '');

  static Map<String, dynamic> _plainifyContext(StructGeneratorContext context) {
    return {
      'rootName': context.rootName,
      'structPrefix': context.structPrefix,
      'funcPrefix': context.funcPrefix,
      'fieldSeparator': context.fieldSeparator,
      'listSuffix': context.listSuffix,
      'listItemSuffix': context.listItemSuffix,
      'allocSuffix': context.allocSuffix,
      'disposeSuffix': context.disposeSuffix,
      'getInfix': context.getInfix,
      'getListItemSuffix': context.getListItemSuffix,
      'getListSizeSuffix': context.getListSizeSuffix,
      'setInfix': context.setInfix,
      'setListItemSuffix': context.setListItemSuffix,
      'addListItemSuffix': context.addListItemSuffix,
      'growListSuffix': context.growListSuffix,
      'selfVarName': context.selfVarName,
      'charType': context.charType,
      'nullValue': context.nullValue,
      'sizeType': StructSerializer(context.sizeType).serialize(),
      'needExtern': context.needExtern,
    };
  }
}

class ContextDeserializer with DeserializerAssistance {
  final dynamic _data;

  ContextDeserializer(String data) : _data = loadYaml(data);

  ContextDeserializer.fromParsed(this._data);

  StructGeneratorContext deserialize() {
    assertTyped<Map>(_data, name: '#root');

    var sizeType = _data['sizeType'] == null
        ? ContextSerializer.defaultContext.sizeType
        : StructDeserializer.fromParsed(_data['sizeType']).deserialize();

    return StructGeneratorContext(
      rootName: getTypedValue(_data, 'rootName') ??
          ContextSerializer.defaultContext.rootName,
      structPrefix: getTypedValue(_data, 'structPrefix') ??
          ContextSerializer.defaultContext.structPrefix,
      funcPrefix: getTypedValue(_data, 'funcPrefix') ??
          ContextSerializer.defaultContext.funcPrefix,
      fieldSeparator: getTypedValue(_data, 'fieldSeparator') ??
          ContextSerializer.defaultContext.fieldSeparator,
      listSuffix: getTypedValue(_data, 'listSuffix') ??
          ContextSerializer.defaultContext.listSuffix,
      listItemSuffix: getTypedValue(_data, 'listItemSuffix') ??
          ContextSerializer.defaultContext.listItemSuffix,
      allocSuffix: getTypedValue(_data, 'allocSuffix') ??
          ContextSerializer.defaultContext.allocSuffix,
      disposeSuffix: getTypedValue(_data, 'disposeSuffix') ??
          ContextSerializer.defaultContext.disposeSuffix,
      getInfix: getTypedValue(_data, 'getInfix') ??
          ContextSerializer.defaultContext.getInfix,
      getListItemSuffix: getTypedValue(_data, 'getListItemSuffix') ??
          ContextSerializer.defaultContext.getListItemSuffix,
      getListSizeSuffix: getTypedValue(_data, 'getListSizeSuffix') ??
          ContextSerializer.defaultContext.getListSizeSuffix,
      setInfix: getTypedValue(_data, 'setInfix') ??
          ContextSerializer.defaultContext.setInfix,
      setListItemSuffix: getTypedValue(_data, 'setListItemSuffix') ??
          ContextSerializer.defaultContext.setListItemSuffix,
      addListItemSuffix: getTypedValue(_data, 'addListItemSuffix') ??
          ContextSerializer.defaultContext.addListItemSuffix,
      growListSuffix: getTypedValue(_data, 'growListSuffix') ??
          ContextSerializer.defaultContext.growListSuffix,
      selfVarName: getTypedValue(_data, 'selfVarName') ??
          ContextSerializer.defaultContext.selfVarName,
      charType: getTypedValue(_data, 'charType') ??
          ContextSerializer.defaultContext.charType,
      nullValue: getTypedValue(_data, 'nullValue') ??
          ContextSerializer.defaultContext.nullValue,
      sizeType: assertTyped(sizeType),
      needExtern: getTypedValue(_data, 'needExtern') ??
          ContextSerializer.defaultContext.needExtern,
      useWString: getTypedValue(_data, 'useWString') ??
          ContextSerializer.defaultContext.useWString,
    );
  }
}

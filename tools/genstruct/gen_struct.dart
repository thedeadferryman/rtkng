/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:collection';

import 'struct_def.dart';

abstract class StructGeneratorBase {
  final StructGeneratorContext _context;
  final StructDefObject _struct;
  final String _name;

  StructGeneratorBase(this._context, this._struct, String? name)
      : _name = (name ?? _context.rootName);

  String _fieldTypeName(String name, StructDef def) {
    if (def is StructDefComplex) {
      return _context.structPrefix + _childStructName(name, def);
    }
    if (def is StructDefString) {
      return '${_context.charType}*';
    }

    return def.typedef;
  }

  String _fieldType(String name, StructDef def) {
    return _fieldTypeName(name, def) + ((def is StructDefComplex) ? '*' : '');
  }

  String _childStructName(String key, StructDefComplex def) {
    if (def is StructDefObject) {
      return '$_name${_context.fieldSeparator}$key';
    }
    if (def is StructDefList) {
      return '$_name${_context.fieldSeparator}$key${_context.listSuffix}';
    }

    return _name;
  }

  Iterable<T> _mapStructDeps<T>(
      T Function(String key, StructDefComplex value) it) {
    return _struct.fields.entries.map((e) {
      var name = e.key;
      var def = e.value;

      if (def is StructDefObject) {
        return [it(name, def)];
      }

      if (def is StructDefList) {
        return [it(name, def)];
      }

      return <T>[];
    }).expand((e) => e);
  }

  String generate() {
    var deps = _generateDeps().toList();
    var self = _generateSelf();

    return (deps + [self]).join('\n\n');
  }

  String get _structDefName => '${_context.structPrefix}$_name';

  String get internals => '';

  Iterable<String> _generateDeps() => [];

  String _generateSelf() => '';
}

class StructDefGenerator extends StructGeneratorBase {
  StructDefGenerator(StructGeneratorContext context, StructDefObject struct,
      [String? name])
      : super(context, struct, name);

  String _generateListDef(String name, StructDefList def) {
    var deps = '';
    var itemDef = def.item;

    if (itemDef is StructDefList) {
      deps = _generateListDef('$name${_context.listItemSuffix}', itemDef);
    }
    if (itemDef is StructDefObject) {
      deps = _fieldGenerator('$name${_context.listItemSuffix}', itemDef)
          .generate();
    }

    var fields = HashMap<String, String>.identity();

    fields['items'] =
        _fieldType('$name${_context.listItemSuffix}', itemDef) + '*';
    fields['buf_size'] = _context.sizeType.typedef;
    fields['size'] = _context.sizeType.typedef;

    var body = fields.entries.map((e) => '${e.value} ${e.key};').join('\n\t');

    var fullName = _fieldTypeName(name, def);

    var self = 'typedef struct {\n\t$body\n} $fullName;';

    return [deps, self].join('\n\n');
  }

  @override
  String _generateSelf() {
    var fields = HashMap<String, String>.identity();

    _struct.fields.forEach((name, def) => fields[name] = _fieldType(name, def));

    var body = fields.entries.map((e) => '${e.value} ${e.key};').join('\n\t');

    return 'typedef struct {\n\t$body\n} $_structDefName;';
  }

  @override
  Iterable<String> _generateDeps() => _mapStructDeps((name, def) {
        if (def is StructDefObject) {
          return _fieldGenerator(name, def).generate();
        }

        if (def is StructDefList) {
          return _generateListDef(name, def);
        }

        return '';
      });

  StructDefGenerator _fieldGenerator(String key, StructDefObject def) =>
      StructDefGenerator(_context, def, _childStructName(key, def));
}

abstract class StructFuncGenerator extends StructGeneratorBase {
  final bool _withBody;

  String get listBufSize =>
      'SGCONST__${_context.rootName}__INTERNAL__LIST_BUF_SIZE';

  String get stringAllocator =>
      'sgfun__${_context.rootName}__internal__string_alloc';

  StructFuncGenerator(StructGeneratorContext context, StructDefObject struct,
      this._withBody, String? name)
      : super(context, struct, name);
}

class StructAllocGenerator extends StructFuncGenerator {
  StructAllocGenerator(StructGeneratorContext context, StructDefObject struct,
      {bool withBody = true, String? name})
      : super(context, struct, withBody, name);

  String get allocatorName =>
      '${_context.funcPrefix}$_name${_context.allocSuffix}';

  String _allocSelf() {
    return '$_structDefName* self = malloc(sizeof($_structDefName))';
  }

  String _fieldAllocName(String name, StructDefComplex def) =>
      _context.funcPrefix + _childStructName(name, def) + _context.allocSuffix;

  String _generateListAlloc(String name, StructDefList def) {
    var deps = '';
    var itemDef = def.item;

    if (itemDef is StructDefList) {
      deps = _generateListAlloc('$name${_context.listItemSuffix}', itemDef);
    }
    if (itemDef is StructDefObject) {
      deps = _fieldGenerator('$name${_context.listItemSuffix}', itemDef)
          .generate();
    }

    var fields = HashMap<String, String>.identity();

    fields['items'] = 'malloc($listBufSize * sizeof(' +
        _fieldTypeName('$name${_context.listItemSuffix}', def.item) +
        '))';
    fields['buf_size'] = listBufSize;
    fields['size'] = '0';

    var lines = List<String>.empty(growable: true);

    lines.add(_listAllocSelf(name, def));

    fields.forEach((key, value) {
      lines.add('${_context.selfVarName}->$key = $value');
    });

    lines.add('return ${_context.selfVarName}');

    var body = lines.join(';\n\t');

    var head = _fieldType(name, def) + ' ' + _fieldAllocName(name, def) + '()';

    var self = _withBody ? '$head {\n\t$body;\n}' : '$head;';

    return [deps, self].join('\n\n');
  }

  String _listAllocSelf(String name, StructDefList def) {
    return _fieldType(name, def) +
        ' ${_context.selfVarName} = malloc(sizeof(' +
        _fieldTypeName(name, def) +
        '))';
  }

  @override
  Iterable<String> _generateDeps() {
    return _mapStructDeps((key, def) {
      if (def is StructDefObject) {
        return _fieldGenerator(key, def).generate();
      }

      if (def is StructDefList) {
        return _generateListAlloc(key, def);
      }

      return '';
    });
  }

  @override
  String _generateSelf() {
    var fields = _struct.fields.map((key, def) {
      if (def is StructDefObject) {
        return MapEntry(key, _fieldAllocName(key, def) + '()');
      } else if (def is StructDefList) {
        return MapEntry(key, _fieldAllocName(key, def) + '()');
      } else if (def is StructDefString) {
        return MapEntry(key, '$stringAllocator()');
      } else {
        return MapEntry(key, '0');
      }
    });

    var lines = List<String>.empty(growable: true);

    lines.add(_allocSelf());

    fields.forEach((key, value) {
      lines.add('${_context.selfVarName}->$key = $value');
    });

    lines.add('return ${_context.selfVarName}');

    var body = lines.join(';\n\t');

    var head = '$_structDefName* $allocatorName()';

    return _withBody ? '$head {\n\t$body;\n}' : '$head;';
  }

  @override
  String get internals {
    var lbs = 'const ${_context.sizeType.typedef} $listBufSize = 1024;';
    var sallocHead = '${_context.charType}* $stringAllocator()';

    var sallocBody =
        '{\n\t${_context.charType} *${_context.selfVarName} = malloc(sizeof(${_context.charType}));'
        '\n\t${_context.selfVarName}[0] = 0;\n\treturn ${_context.selfVarName};\n}';

    return _withBody ? [lbs, '$sallocHead $sallocBody'].join('\n\n') : '';
  }

  StructAllocGenerator _fieldGenerator(String key, StructDefObject def) =>
      StructAllocGenerator(_context, def,
          name: _childStructName(key, def), withBody: _withBody);
}

class StructGetterGenerator extends StructFuncGenerator {
  StructGetterGenerator(StructGeneratorContext context, StructDefObject struct,
      {bool withBody = true, String? name})
      : super(context, struct, withBody, name);

  String _fieldGetterName(String key) =>
      '${_context.funcPrefix}$_name${_context.getInfix}$key';

  String _fieldGetterHead(String key, StructDef def) =>
      _fieldType(key, def) +
      ' ' +
      _fieldGetterName(key) +
      '($_structDefName* ${_context.selfVarName})';

  String _fieldGetterBody(String key) =>
      '{\n\treturn ${_context.selfVarName}->$key;\n}';

  String _generateListGetters(String key, StructDefList def) {
    var deps = '';
    var itemDef = def.item;

    if (itemDef is StructDefList) {
      deps = _generateListGetters('$key${_context.listItemSuffix}', itemDef);
    }
    if (itemDef is StructDefObject) {
      deps =
          _fieldGenerator('$key${_context.listItemSuffix}', itemDef).generate();
    }

    var getters = List<String>.empty(growable: true);

    var elemHead = _fieldType('$key${_context.listItemSuffix}', itemDef) +
        ' ${_context.funcPrefix}' +
        _childStructName(key, def) +
        '${_context.getListItemSuffix}(' +
        _fieldType(key, def) +
        ' ${_context.selfVarName}, ${_context.sizeType.typedef} i)';

    var elemBody =
        '{\n\tif (${_context.selfVarName}->size <= i) return ${_context.nullValue};'
        '\n\treturn ${_context.selfVarName}->items[i];\n}';

    var sizeHead = _context.sizeType.typedef +
        ' ${_context.funcPrefix}' +
        _childStructName(key, def) +
        '${_context.getListSizeSuffix}(' +
        _fieldType(key, def) +
        ' ${_context.selfVarName})';

    var sizeBody = '{\n\treturn ${_context.selfVarName}->size;\n}';

    getters.add(deps);

    getters.add(_withBody ? '$elemHead $elemBody' : '$elemHead;');
    getters.add(_withBody ? '$sizeHead $sizeBody' : '$sizeHead;');

    return getters.join('\n\n');
  }

  @override
  Iterable<String> _generateDeps() {
    return _mapStructDeps((key, def) {
      if (def is StructDefObject) {
        return [_fieldGenerator(key, def).generate()];
      }

      if (def is StructDefList) {
        return [_generateListGetters(key, def)];
      }

      return <String>[];
    }).expand((e) => e);
  }

  @override
  String _generateSelf() {
    var self = _struct.fields.entries.map((e) {
      var key = e.key;
      var def = e.value;

      var head = _fieldGetterHead(key, def);
      var body = _fieldGetterBody(key);

      return _withBody ? '$head $body' : '$head;';
    });

    return self.join('\n\n');
  }

  StructGetterGenerator _fieldGenerator(String key, StructDefObject def) =>
      StructGetterGenerator(_context, def,
          name: _childStructName(key, def), withBody: _withBody);
}

class StructSetterGenerator extends StructFuncGenerator {
  StructSetterGenerator(StructGeneratorContext context, StructDefObject struct,
      {bool withBody = true, String? name})
      : super(context, struct, withBody, name);

  String _fieldSetterName(String key) =>
      '${_context.funcPrefix}$_name${_context.setInfix}$key';

  String _fieldSetterHead(String key, StructDef def) =>
      'void ' +
      _fieldSetterName(key) +
      '(' +
      _structDefName +
      '* ${_context.selfVarName}, const ' +
      _fieldType(key, def) +
      ' value)';

  String _fieldSetterBody(String key) =>
      '{\n\t${_context.selfVarName}->$key = value;\n}';

  Map<String, String> get _libcstring => {
        'strlen': _context.useWString ? 'wcslen' : 'strlen',
        'strcpy': _context.useWString ? 'wcscpy' : 'strcpy',
      };

  String _stringSetterBody(String key) =>
      '{\n\t${_context.sizeType.typedef} sz = '
      '(${_libcstring['strlen']}(value) + 1) * sizeof(${_context.charType});'
      '\n\tfree(${_context.selfVarName}->$key);'
      '\n\t${_context.selfVarName}->$key = malloc(sz);'
      '\n\tmemset(${_context.selfVarName}->$key, 0, sz);'
      '\n\t${_libcstring['strcpy']}(${_context.selfVarName}->$key, value);\n}';

  String _listGrowName(String name, StructDefList def) =>
      _context.funcPrefix +
      _childStructName(name, def) +
      _context.growListSuffix;

  String _listGrowHead(String name, StructDefList def) =>
      'void ' +
      _listGrowName(name, def) +
      '(' +
      _fieldType(name, def) +
      ' ${_context.selfVarName})';

  String _listGrowBody(String name, StructDefList def) =>
      '{\n\t${_context.selfVarName}->buf_size += ' +
      listBufSize +
      ';\n\t${_context.selfVarName}->items = ' +
      'realloc(${_context.selfVarName}->items, ${_context.selfVarName}->buf_size * sizeof(' +
      _fieldType('$name${_context.listItemSuffix}', def.item) +
      '));\n}';

  String _generateListSetters(String key, StructDefList def) {
    var deps = '';
    var itemDef = def.item;

    if (itemDef is StructDefList) {
      deps = _generateListSetters('$key${_context.listItemSuffix}', itemDef);
    }
    if (itemDef is StructDefObject) {
      deps =
          _fieldGenerator('$key${_context.listItemSuffix}', itemDef).generate();
    }

    var setters = List<String>.empty(growable: true);

    var elemHead = 'void ' +
        _context.funcPrefix +
        _childStructName(key, def) +
        '${_context.setListItemSuffix}(' +
        _fieldType(key, def) +
        ' ${_context.selfVarName}, ${_context.sizeType.typedef} i, ' +
        _fieldType('$key${_context.listItemSuffix}', itemDef) +
        ' value)';

    var elemBody = '{\n\tif (${_context.selfVarName}->size <= i) return;'
        '\n\t${_context.selfVarName}->items[i] = value;\n}';

    var addHead = 'void ' +
        _context.funcPrefix +
        _childStructName(key, def) +
        '${_context.addListItemSuffix}(' +
        _fieldType(key, def) +
        ' ${_context.selfVarName}, ' +
        _fieldType('$key${_context.listItemSuffix}', itemDef) +
        ' value)';

    var addBody =
        '{\n\tif ((${_context.selfVarName}->size + 1) >= ${_context.selfVarName}->buf_size) {\n\t\t' +
            _listGrowName(key, def) +
            '(${_context.selfVarName});\n\t}' +
            '\n\t${_context.selfVarName}->items[${_context.selfVarName}->size] = value;' +
            '\n\t${_context.selfVarName}->size++;\n}';

    var growHead = _listGrowHead(key, def);
    var growBody = _listGrowBody(key, def);

    setters.add(deps);

    setters.add(_withBody ? '$elemHead $elemBody' : '$elemHead;');
    setters.add(_withBody ? '$growHead $growBody' : '$growHead;');
    setters.add(_withBody ? '$addHead $addBody' : '$addHead;');

    return setters.join('\n\n');
  }

  @override
  Iterable<String> _generateDeps() {
    return _mapStructDeps((key, def) {
      if (def is StructDefObject) {
        return [_fieldGenerator(key, def).generate()];
      }

      if (def is StructDefList) {
        return [_generateListSetters(key, def)];
      }

      return <String>[];
    }).expand((e) => e);
  }

  @override
  String _generateSelf() {
    var self = _struct.fields.entries.map((e) {
      var key = e.key;
      var def = e.value;

      var head = _fieldSetterHead(key, def);
      var body = (def is StructDefString)
          ? _stringSetterBody(key)
          : _fieldSetterBody(key);

      return _withBody ? '$head $body' : '$head;';
    });

    return self.join('\n\n');
  }

  StructSetterGenerator _fieldGenerator(String key, StructDefObject def) =>
      StructSetterGenerator(_context, def,
          name: _childStructName(key, def), withBody: _withBody);
}

class StructDisposerGenerator extends StructFuncGenerator {
  StructDisposerGenerator(
      StructGeneratorContext context, StructDefObject struct,
      {bool withBody = true, String? name})
      : super(context, struct, withBody, name);

  String get disposerName =>
      '${_context.funcPrefix}$_name${_context.disposeSuffix}';

  String _fieldDisposerName(String name, StructDefComplex def) =>
      _context.funcPrefix +
      _childStructName(name, def) +
      _context.disposeSuffix;

  String? _listItemDisposer(String key, StructDef def, String varb) {
    if (def is StructDefComplex) {
      return _fieldDisposerName('$key${_context.listItemSuffix}', def) +
          '($varb)';
    } else if (def is StructDefString) {
      return 'free($varb)';
    } else {
      return null;
    }
  }

  String _generateListDisposer(String name, StructDefList def) {
    var deps = '';
    var itemDef = def.item;

    if (itemDef is StructDefList) {
      deps = _generateListDisposer('$name${_context.listItemSuffix}', itemDef);
    }
    if (itemDef is StructDefObject) {
      deps = _fieldGenerator('$name${_context.listItemSuffix}', itemDef)
          .generate();
    }

    var itemDisposer =
        _listItemDisposer(name, itemDef, '${_context.selfVarName}->items[pos]');

    var loop = '';

    if (itemDisposer != null) {
      loop =
          'for(uint64_t pos = 0; pos < ${_context.selfVarName}->size; pos++) {\n\t\t' +
              itemDisposer +
              ';\n\t}';
    }

    var lines = List<String>.empty(growable: true);

    lines.add(loop);

    lines.add('free(${_context.selfVarName})');

    var body = lines.join(';\n\t');

    var head = 'void ' +
        _fieldDisposerName(name, def) +
        '(' +
        _fieldType(name, def) +
        ' ${_context.selfVarName})';

    var self = _withBody ? '$head {\n\t$body;\n}' : '$head;';

    return [deps, self].join('\n\n');
  }

  @override
  Iterable<String> _generateDeps() {
    return _mapStructDeps((key, def) {
      if (def is StructDefObject) {
        return _fieldGenerator(key, def).generate();
      }

      if (def is StructDefList) {
        return _generateListDisposer(key, def);
      }

      return '';
    });
  }

  @override
  String _generateSelf() {
    var lines = _struct.fields.entries
        .map((e) {
          var key = e.key;
          var def = e.value;

          if (def is StructDefComplex) {
            return [
              _fieldDisposerName(key, def) + '(${_context.selfVarName}->$key)'
            ];
          } else if (def is StructDefString) {
            return ['free(${_context.selfVarName}->$key)'];
          } else {
            return [];
          }
        })
        .expand((e) => e)
        .toList(growable: true);

    lines.add('free(${_context.selfVarName})');

    var body = lines.join(';\n\t');

    var head = 'void $disposerName($_structDefName *${_context.selfVarName})';

    return _withBody ? '$head {\n\t$body;\n}' : '$head;';
  }

  StructDisposerGenerator _fieldGenerator(String key, StructDefObject def) =>
      StructDisposerGenerator(_context, def,
          name: _childStructName(key, def), withBody: _withBody);
}

class StructOutputType {
  static const headerOnly = StructOutputType.__(1);
  static const header = StructOutputType.__(2);
  static const source = StructOutputType.__(4);

  final int _id;

  const StructOutputType.__(this._id);

  bool get withBody => _id != header._id;

  bool get withStructDef => _id != source._id;

  bool get withHeaderFn => _id == source._id;

  bool get withExtern => _id != source._id;

  bool get withGuards => _id != source._id;
}

class StructGeneratorContext {
  final String rootName;
  final String structPrefix;
  final String funcPrefix;
  final String fieldSeparator;
  final String listSuffix;
  final String listItemSuffix;
  final String allocSuffix;
  final String disposeSuffix;
  final String getInfix;
  final String getListItemSuffix;
  final String getListSizeSuffix;
  final String setInfix;
  final String setListItemSuffix;
  final String addListItemSuffix;
  final String growListSuffix;
  final String selfVarName;
  final String charType;
  final String nullValue;
  final StructDefInteger sizeType;
  final bool needExtern;
  final bool useWString;

  StructGeneratorContext({
    required this.rootName,
    this.structPrefix = 'sgstruct_',
    this.funcPrefix = 'sgfun_',
    this.fieldSeparator = '__',
    this.listSuffix = '_ar',
    this.listItemSuffix = '_it',
    this.allocSuffix = '_alloc',
    this.disposeSuffix = '_dispose',
    this.getInfix = '_get_',
    this.getListItemSuffix = '_at',
    this.getListSizeSuffix = '_size',
    this.setInfix = '_set_',
    this.setListItemSuffix = '_set_at',
    this.addListItemSuffix = '_push',
    this.growListSuffix = '_grow',
    this.selfVarName = 'self',
    this.charType = 'uint8_t',
    this.nullValue = 'NULL',
    this.sizeType = const StructDefInteger(64, false),
    this.needExtern = true,
    this.useWString = false,
  });
}

class StructGenerator {
  final StructDefObject _struct;
  final StructGeneratorContext _context;

  StructGenerator(this._context, this._struct);

  String get _name => _context.rootName;

  String generate(
      {String? headerFileName,
      StructOutputType outputType = StructOutputType.headerOnly}) {
    if (outputType.withHeaderFn && (headerFileName == null)) {
      headerFileName = '$_name.h';
    }

    var generators = [
      (outputType.withStructDef
          ? [StructDefGenerator(_context, _struct)]
          : <StructGeneratorBase>[]),
      [StructAllocGenerator(_context, _struct, withBody: outputType.withBody)],
      [StructGetterGenerator(_context, _struct, withBody: outputType.withBody)],
      [StructSetterGenerator(_context, _struct, withBody: outputType.withBody)],
      [
        StructDisposerGenerator(_context, _struct,
            withBody: outputType.withBody)
      ],
    ].expand((e) => e);

    var linkedHeader =
        outputType.withHeaderFn ? '#include "$headerFileName"\n' : '';

    var body = '';

    var guardName = 'SGLIB__' + _context.rootName.toUpperCase();

    if (outputType.withGuards) {
      body += '#ifndef $guardName\n#define $guardName\n\n';
    }

    body +=
        '#include <stdint.h>\n#include <stdlib.h>\n#include <string.h>\n\n$linkedHeader\n';

    if (outputType.withExtern && _context.needExtern) {
      body += '#ifdef __cplusplus\nextern "C" {\n#endif';
      body += '\n\n';
    }

    body += generators.map((e) => e.internals).join('\n\n');
    body += '\n\n';

    body += generators.map((e) => e.generate()).join('\n\n');
    body += '\n\n';

    if (outputType.withExtern && _context.needExtern) {
      body += '#ifdef __cplusplus\n};\n#endif';
    }

    if (outputType.withGuards) {
      body += '\n#endif // $guardName\n';
    }

    body += '\n';

    body = body.replaceAll(RegExp('\n\n\n+'), '\n\n');

    return body;
  }
}

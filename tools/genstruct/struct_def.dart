/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:collection';

class StructDef {
  String get typedef => 'void*';

  const StructDef();
}

class StructDefComplex extends StructDef {}

class StructDefObject extends StructDefComplex {
  final Map<String, StructDef> _data;

  Map<String, StructDef> get fields => _data;

  StructDefObject.__(this._data);

  StructDefObject() : this.__(HashMap.identity());

  void add(String name, StructDef value) {
    _data[name] = value;
  }

  StructDefObject extend() {
    return StructDefObject.__(HashMap.from(_data));
  }
}

class StructDefList extends StructDefComplex {
  final StructDef _item;

  static String get definition => '';

  StructDef get item => _item;

  StructDefList(this._item);
}

class StructDefString extends StructDef {
  @override
  String get typedef => 'wchar_t*';

  const StructDefString.id();
}

class StructDefInteger extends StructDef {
  static const supportedWidths = [8, 16, 32, 64];

  final int _width;

  final bool _sign;

  int get width => _width;

  bool get sign => _sign;

  @override
  String get typedef => (_sign ? '' : 'u') + 'int${_width}_t';

  const StructDefInteger(this._width, this._sign);
}

class StructDefDouble extends StructDef {
  @override
  String get typedef => 'double';

  const StructDefDouble.id();
}

class Struct {
  static const StructDefString string = StructDefString.id();
  static const StructDefDouble double = StructDefDouble.id();

  static StructDefObject object() => StructDefObject();

  static StructDefInteger sint(int width) => StructDefInteger(width, true);

  static StructDefInteger uint(int width) => StructDefInteger(width, false);

  static StructDefList list(StructDef item) => StructDefList(item);
}

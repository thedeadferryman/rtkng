/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:ffi';
import 'dart:io';

import 'package:ffi/ffi.dart';
import 'package:rtkng/util/sys/ffi.dart';

import 'bindings.dart' as lib;
import 'media_info.dart';

class DartAVconvLibrary {
  static const String dylibName = 'libdartavconv';

  static lib.DartAVconv? _lib;

  static void initialize() {
    if (_lib != null) return;

    var dylib = DynamicLibrary.open(dylibName);
    _lib = lib.DartAVconv(dylib);
  }

  static lib.DartAVconv get instance {
    initialize();

    return _lib!;
  }
}

mixin _CDEFObject {
  static lib.DartAVconv get _libStatic => DartAVconvLibrary.instance;

  lib.DartAVconv get _lib => _libStatic;
}

extension _ToDartString8 on Pointer<Int8> {
  String toDartString({int? length}) =>
      cast<Utf8>().toDartString(length: length);
}

class MediaAnalyzerError extends FFIError {
  static const _codeToMessage = {
    lib.InfoStatus.IS_NO_INPUT_FILE: "Couldn't open input file",
    lib.InfoStatus.IS_NO_INPUT_STREAMS: "Couldn't read stream info for file"
  };

  static final _codeOffset = FFIError.nextOffset;

  @override
  int get codeOffset => _codeOffset;

  @override
  final String message;
  @override
  final int code;

  MediaAnalyzerError(this.message, this.code);

  factory MediaAnalyzerError.fromCode(int code) => MediaAnalyzerError(
        _codeToMessage[code] ?? 'Unexpected libav error',
        code,
      );
}

class MediaInfoParser with _CDEFObject {
  MediaInfo analyzeMedia(File mediaFile) {
    var path = mediaFile.absolute.path;

    var libInfo = _lib.analyzeMedia(path.toNativeUtf8().cast());

    if (libInfo.status != lib.InfoStatus.IS_OK) {
      throw MediaAnalyzerError.fromCode(libInfo.status);
    }

    var mediaInfo = _parseMediaInfo(libInfo);

    _lib.MediaInfo_dispose(libInfo);

    return mediaInfo;
  }

  MediaInfo _parseMediaInfo(lib.MediaInfo libInfo) {
    return MediaInfo.parse(
        container: _parseContainerInfo(libInfo.container),
        streams: _parseStreams(libInfo));
  }

  ContainerInfo _parseContainerInfo(lib.ContainerInfo container) {
    return ContainerInfo(
      filename: container.filename.toDartString(),
      format: container.format.toDartString(),
      duration: Duration(seconds: container.duration),
      size: container.fileSize,
    );
  }

  List<StreamInfo> _parseStreams(lib.MediaInfo libInfo) => [
        _parseVideoStreams(libInfo.video),
        _parseAudioStreams(libInfo.audio),
      ].expand((e) => e).toList();

  List<VideoInfo> _parseVideoStreams(lib.VideoInfos videos) {
    var res = <VideoInfo>[];

    for (var i = 0; i < videos.size; i++) {
      var item = videos.items.elementAt(i).ref;
      res.add(VideoInfo(
        bitrate: item.bitrate,
        duration: Duration(seconds: item.duration),
        codec: _parseCodec(item.codec),
        resolution: ImageResolution(
          width: item.resolution.width,
          height: item.resolution.height,
        ),
        pixelFormat: item.pixFormat.toDartString(),
        frameRate: item.fps,
      ));
    }

    return res;
  }

  List<AudioInfo> _parseAudioStreams(lib.AudioInfos videos) {
    var res = <AudioInfo>[];

    for (var i = 0; i < videos.size; i++) {
      var item = videos.items.elementAt(i).ref;
      res.add(AudioInfo(
        bitrate: item.bitrate,
        duration: Duration(seconds: item.duration),
        codec: _parseCodec(item.codec),
        sampleRate: item.sampleRate,
      ));
    }

    return res;
  }

  Codec _parseCodec(lib.Codec codec) => Codec(
        name: codec.name.toDartString(),
        tag: codec.tag.toDartString(),
      );
}

class CollectorResponse {
  final int code;
  final List<InternalSnapshotResponseItem> snaps;

  CollectorResponse.__(this.code, this.snaps);
}

class SnapshotCollectorError extends FFIError {
  static const _codeToMessage = {
    lib.SnapshotStatus.SS_NO_INPUT_FILE: "Couldn't open input file",
    lib.SnapshotStatus.SS_NO_VIDEO_STREAM: "Couldn't find video stream",
    lib.SnapshotStatus.SS_ERR_OUTPUT_FILE: "Couldn't open one of output files",
    lib.SnapshotStatus.SS_ERR_WRITE_FRAME:
        'Failed writing frame to output file',
    lib.SnapshotStatus.SS_ERR_WRITE_HEADER:
        'Failed writing header to output file',
    lib.SnapshotStatus.SS_ERR_WRITE_TRAILER:
        'Failed writing trailer to output file',
    lib.SnapshotStatus.SS_ERR_OUT_OF_RANGE: 'Timecode is out of range',
  };

  static final _codeOffset = FFIError.nextOffset;

  @override
  int get codeOffset => _codeOffset;

  @override
  final String message;
  @override
  final int code;

  SnapshotCollectorError(this.message, this.code);

  factory SnapshotCollectorError.fromCode(int code) => SnapshotCollectorError(
        _codeToMessage[code] ?? 'Unexpected libav error',
        code,
      );
}

class InternalSnapshotResponseItem with _CDEFObject {
  final String outPath;
  final int timecode;

  InternalSnapshotResponseItem(this.outPath, this.timecode);

  InternalSnapshotResponseItem.fromNative(lib.Snapshot item)
      : this(item.path.toDartString(), item.timecode);
}

class InternalSnapshotCollector with _CDEFObject {
  static void Function(int, int) onProgress = (c, t) {};

  static R withOnProgress<R>(
      void Function(int, int) onProgress, R Function() body) {
    var oldOnPr = InternalSnapshotCollector.onProgress;
    InternalSnapshotCollector.onProgress = onProgress;

    var r = body();

    InternalSnapshotCollector.onProgress = oldOnPr;

    return r;
  }

  static void _onProgressWrapper(int current, int total) =>
      onProgress(current, total);

  List<InternalSnapshotResponseItem> collect(
    String inputFile,
    String outDir,
    int count,
  ) {
    var res = _lib.collectSnapshots(
      inputFile.toNativeUtf8().cast(),
      outDir.toNativeUtf8().cast(),
      count,
      Pointer.fromFunction(_onProgressWrapper),
    );

    if (res.status != lib.SnapshotStatus.SS_OK) {
      throw MediaAnalyzerError.fromCode(res.status);
    }

    var parseSnaps = _parseSnaps(res.snaps);

    _lib.SnapshotResult_dispose(res);

    return parseSnaps;
  }

  InternalSnapshotResponseItem takePrecise(
    String inputFile,
    String outputFile,
    int time,
  ) {
    var res = _lib.takePreciseSnapshot(
      inputFile.toNativeUtf8().cast(),
      time,
      outputFile.toNativeUtf8().cast(),
    );

    if (res.status != lib.SnapshotStatus.SS_OK) {
      throw MediaAnalyzerError.fromCode(res.status);
    }

    return InternalSnapshotResponseItem.fromNative(res);
  }

  List<InternalSnapshotResponseItem> _parseSnaps(lib.Snapshots snaps) {
    var res = <InternalSnapshotResponseItem>[];

    for (var i = 0; i < snaps.size; i++) {
      res.add(InternalSnapshotResponseItem.fromNative(
          snaps.items.elementAt(i).ref));
    }

    return res;
  }
}

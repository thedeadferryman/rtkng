/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'package:collection/collection.dart';
import 'package:rtkng/util/duration.dart';
import 'package:rtkng/util/filesize.dart';

class Codec {
  final String name;
  final String tag;

  const Codec({required this.name, required this.tag});

  @override
  String toString() => '$tag/$name';
}

class ImageResolution {
  final int width;
  final int height;

  const ImageResolution({required this.width, required this.height});

  @override
  String toString() => '${width}x$height';
}

abstract class StreamInfo {
  final Codec? codec;
  final int bitrate;
  final Duration duration;

  const StreamInfo(this.codec, this.bitrate, this.duration);

  Map<dynamic, dynamic> toMap() => ({
        'codec': codec,
        'bitrate': bitrate,
        'duration': duration.toTimecodeString(detailed: true),
      });
}

class AudioInfo extends StreamInfo {
  final double? sampleRate;

  const AudioInfo(
      {this.sampleRate,
      int bitrate = 0,
      Duration duration = Duration.zero,
      Codec? codec})
      : super(codec, bitrate, duration);

  @override
  String toString() => ({
        ...toMap(),
        'sampleRate': sampleRate,
      }).toString();
}

class VideoInfo extends StreamInfo {
  final ImageResolution? resolution;
  final double? frameRate;
  final String? pixelFormat;

  const VideoInfo(
      {this.frameRate,
      this.resolution,
      this.pixelFormat,
      int bitrate = 0,
      Duration duration = Duration.zero,
      Codec? codec})
      : super(codec, bitrate, duration);

  @override
  String toString() => ({
        ...toMap(),
        'resolution': resolution,
        'frameRate': frameRate,
        'pixelFormat': pixelFormat,
      }).toString();
}

class Multiplex<T extends StreamInfo> extends DelegatingList<T> {
  Multiplex(Iterable<T> e) : super(e.toList());

  Multiplex.empty() : this(<T>[]);
}

class ContainerInfo {
  final String? filename;
  final String? format;
  final Duration? duration;
  final int? size;

  const ContainerInfo({this.filename, this.format, this.duration, this.size});

  @override
  String toString() => ({
        'filename': filename,
        'format': format,
        'duration': duration?.toTimecodeString(),
        'size': humanizeBytesize(size ?? 0),
      }).toString();
}

class MediaInfo {
  final ContainerInfo container;
  final Multiplex<AudioInfo> audio;
  final Multiplex<VideoInfo> video;

  MediaInfo._create(this.container, this.audio, this.video);

  static MediaInfo parse(
      {required ContainerInfo container,
      required Iterable<StreamInfo> streams}) {
    var video = Multiplex<VideoInfo>.empty();
    var audio = Multiplex<AudioInfo>.empty();

    for(var stream in streams) {
      if (stream is VideoInfo) video.add(stream);
      if (stream is AudioInfo) audio.add(stream);
    }

    return MediaInfo._create(container, audio, video);
  }

  @override
  String toString() => ({
        'container': container,
        'audio': audio,
        'video': video,
      }).toString();
}

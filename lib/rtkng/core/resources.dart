/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'context.dart';

enum ResourceType { image, font, template }

class ResourceService {
  final AppContext _ctx;

  factory ResourceService(AppContext context) => ResourceService._init(context);

  ResourceService._init(this._ctx);

  String getResourcePath(ResourceType resourceType, String resourceName) {
    return '${_ctx.resourcesPath}/${_resTypeString(resourceType)}/$resourceName';
  }

  String _resTypeString(ResourceType resType) {
    switch (resType) {
      case ResourceType.image:
        return 'images';
      case ResourceType.font:
        return 'fonts';
      default:
        return 'misc';
    }
  }
}

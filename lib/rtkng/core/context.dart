/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:io';

import 'package:args/args.dart';
import 'package:logging/logging.dart';
import 'package:rtkng/rtkng/app_properties.dart';

import '../cli.dart';

class GlobalOptions {
  final Level logLevel;
  final IOSink outputSink;

  GlobalOptions(this.logLevel, this.outputSink);

  static IOSink _openLogSink(String sinkId) {
    switch (sinkId) {
      case '-':
      case 'stdout':
        return stdout;
      case 'stderr':
        return stderr;
      default:
        return File(sinkId).openWrite();
    }
  }

  factory GlobalOptions.parse(ArgResults args, {required Logger logger}) {
    var llName = args['verbose'];

    var logLevel = Level.LEVELS.firstWhere(
      (element) => element.name == llName.toString().toUpperCase(),
      orElse: () {
        logger.warning("Invalid verbosity level '$llName', assuming 'info'");
        return Level.INFO;
      },
    );

    var logSink = _openLogSink(ArgHelper.assertType(args['log-output']));

    return GlobalOptions(logLevel, logSink);
  }
}

class AppContext {
  final List<String> binaryPaths;
  final String resourcesPath;
  final String? tempDir;
  final int parallelLimit;
  final Logger kernelLogger = Logger.detached('rtkng');
  late final IOSink outputSink;

  AppContext({
    this.binaryPaths = const [],
    required this.resourcesPath,
    this.tempDir,
    this.parallelLimit = 8,
  }) {
    _configureKernelLogger();
  }

  void _configureRootLogger(GlobalOptions options) {
    var writer = LogWriter(
      options.outputSink,
      doPrintError: () => options.logLevel <= Level.FINE,
      doPrintTrace: () => options.logLevel <= Level.FINER,
    );

    Logger.root.level = options.logLevel;
    Logger.root.onRecord.listen((event) => writer.write(event));
  }

  void _configureKernelLogger() {
    var writer = LogWriter(
      appProperties.kernelLogSink,
      doPrintError: () => appProperties.isDevMode,
      doPrintTrace: () => appProperties.isDevMode,
    );

    kernelLogger.level = appProperties.kernelLogLevel;
    kernelLogger.onRecord.listen((event) => writer.write(event));
  }

  Future<void> applyGlobalOptions(ArgResults globalArgs) async {
    var options = GlobalOptions.parse(globalArgs, logger: kernelLogger);

    _configureRootLogger(options);
    outputSink = options.outputSink;
  }

  @override
  String toString() => ({
        'binaryPaths': binaryPaths,
        'resourcesPath': resourcesPath,
        'tempDir': tempDir,
        'parallelLimit': parallelLimit,
      }).toString();
}

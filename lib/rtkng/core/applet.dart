/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:async';

import 'package:args/args.dart';
import 'package:args/command_runner.dart';
import 'package:logging/logging.dart';
import 'package:rtkng/progressbar/progressbar.dart';
import 'package:rtkng/rtkng/core.dart';
import 'package:version/version.dart';

class AppletInfo {
  final String key;
  final String name;
  final Version? version;
  final String description;

  const AppletInfo({
    required this.key,
    required this.name,
    required this.description,
    this.version,
  });
}

class AppletFailure extends Error {
  final AppletInfo applet;
  final int exitCode;
  final Object? cause;
  final StackTrace? causeTrace;

  AppletFailure(this.applet, this.exitCode, [this.cause, this.causeTrace]);

  @override
  String toString() =>
      "Applet '${applet.key}' exited with non-zero code $exitCode";
}

extension _StyleInjector<R, P> on Applet<R, P> {
  static final _defaultStyle = Style(
    template: Template.fromString(
      '%n [%B{%p}>%w] %f',
    ),
    alignment: Alignment.stretch,
  );

  Style _injectStyle(Style? style, String caption) {
    var targetStyle = (style ?? _defaultStyle);

    return targetStyle.copy(
      renderOptions: targetStyle.renderOptions.copy(
        caption: 'P [rtkng.applets.${info.key}] $caption',
      ),
    );
  }
}

abstract class Applet<R, P> extends Command<void> {
  final AppContext context;

  Applet(this.context);

  AppletInfo get info;

  Logger get logger => Logger('rtkng.applets.${info.key}');

  ProgressBar createProgressBar({
    int total = 100,
    String caption = '',
    Style? style,
  }) =>
      ProgressBar(
        total: total,
        widthHint: 160,
        outputSink: context.outputSink,
        style: _injectStyle(style, caption),
      );

  @override
  String get name => info.key;

  @override
  String get description => info.description;

  @override
  String get summary => info.description;

  @override
  ArgParser get argParser => ArgParser.allowAnything();

  FutureOr<void> init() => null;

  FutureOr<void> shutdown() => null;

  FutureOr<P> parseArgs(ArgResults args);

  Future<void> _processGlobals() {
    var gRes = globalResults;

    if (gRes == null) {
      throw ArgParserException('no args given');
    }

    return context.applyGlobalOptions(gRes);
  }

  @override
  Future<void> run() async {
    await _processGlobals();

    var argRes = argResults;

    if (argRes == null) {
      throw ArgParserException('no args given');
    }

    var args = await parseArgs(argRes);
    await init();
    await execute(args);
    await shutdown();

    return Future.value();
  }

  Future<R> execute(P args);
}

/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:io';

import 'package:file/file.dart';
import 'package:logging/logging.dart';
import 'package:path/path.dart' as p;

mixin _PropertiesBase {
  String get scriptPath => p.canonicalize(p.fromUri(Platform.script));

  String get scriptDirPath => p.dirname(scriptPath);

  String get scriptName => Platform.isWindows
      ? p.basenameWithoutExtension(scriptPath)
      : p.basename(scriptPath);
}

class _AppProperties with _PropertiesBase {
  final isDevMode = false;
  final kernelLogLevel = Level.INFO;

  IOSink get kernelLogSink => stderr;

  String get appRoot => p.canonicalize('$scriptDirPath/..');

  const _AppProperties();
}

const appProperties = _AppProperties();

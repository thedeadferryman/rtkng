/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:async';
import 'dart:io';

import 'package:args/command_runner.dart';
import 'package:logging/logging.dart';
import 'package:rtkng/rtkng.dart';
import 'package:rtkng/util/string.dart' show Tabulate;

import '../core.dart';

class RTKngRunner extends CommandRunner {
  final AppContext _ctx;

  Logger get _logger => _ctx.kernelLogger;

  @override
  final usageFooter = '\n\n${RTKng.copyright.tabulate(1)}\n\n';

  RTKngRunner(this._ctx) : super(appProperties.scriptName, RTKng.description) {
    argParser
      ..addOption(
        'verbose',
        abbr: 'v',
        aliases: ['log-level'],
        help: 'Adjust the verbosity level',
        valueHelp: 'LEVEL',
        allowed: Level.LEVELS.map((e) => e.name.toLowerCase()),
        defaultsTo: Level.INFO.name.toLowerCase(),
      )
      ..addOption(
        'log-output',
        aliases: ['log-to'],
        help: 'Redirect all logging output to PIPE, '
            'where PIPE can be either a path '
            "or '-' (alias of 'stdout') or 'stdout' or 'stderr'."
            "\nHINT: Use e.g. './stdout' if you want to specify "
            "a 'stdout' file but not a pipe.",
        valueHelp: 'PIPE',
        defaultsTo: 'stderr',
      );
  }

  void registerApplets(Iterable<Applet Function(AppContext)> factories) {
    for (var factory in factories) {
      addCommand(factory(_ctx));
    }
  }

  @override
  Future<void> run(Iterable<String> args) async {
    super.run(args).onError((error, stackTrace) {
      if (error is AppletFailure) {
        _logger.severe(
            'Applet exited with code ${error.exitCode}', error, stackTrace);
        if (error.cause != null) {
          _logger.finer('Caused by: ', error.cause, error.causeTrace);
        }
        exit(error.exitCode);
      } else if (error is UsageException) {
        appProperties.kernelLogSink.writeln(error);
        exit(256);
      } else {
        _logger.shout('Unexpected error', error, stackTrace);
        exit(1);
      }
    });

    _logger.fine('Applet exited with code 0');
  }
}

/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:io';

import 'package:ansicolor/ansicolor.dart';
import 'package:logging/logging.dart';

class LogWriter {
  static bool _truthy() => true;

  static bool _falsy() => false;

  final IOSink _logSink;
  final bool Function() doPrintError;
  final bool Function() doPrintTrace;

  LogWriter(
    this._logSink, {
    this.doPrintError = _truthy,
    this.doPrintTrace = _falsy,
  });

  void _writeln(String? line) => line != null ? _logSink.writeln(line) : null;

  void write(LogRecord event) {
    _writeln(_formatFirstLine(event));

    if (doPrintError()) {
      _writeln(_formatError(event));
    }
    if (doPrintTrace()) {
      _writeln(_formatTrace(event));
    }
  }

  bool get _colorized =>
      _logSink is Stdout ? (_logSink as Stdout).supportsAnsiEscapes : false;

  AnsiPen _levelPen(Level level) {
    var pen = AnsiPen();

    if (_colorized) {
      if (level >= Level.SHOUT) {
        pen
          ..red(bg: true)
          ..white(bold: true);
      } else if (level >= Level.SEVERE) {
        pen.red(bold: true);
      } else if (level >= Level.WARNING) {
        pen.xterm(208);
      } else if (level <= Level.FINE) {
        pen.gray(level: 0.7);
      }
    }

    return pen;
  }

  String _formatFirstLine(LogRecord event) {
    var pen = _levelPen(event.level);

    return pen(
      _formatSeverity(event.level) + '[${event.loggerName}] ${event.message}',
    );
  }

  String? _formatError(LogRecord event) {
    var err = event.error;

    var pen = _levelPen(event.level);

    if (err != null) {
      return pen('Error: $err');
    }
  }

  String? _formatTrace(LogRecord event) {
    var trace = event.stackTrace;

    var pen = _levelPen(event.level);

    if (trace != null) {
      return pen('--- BEGIN TRACE ---') +
          '\n\n' +
          trace.toString() +
          '\n' +
          pen('--- END TRACE ---');
    }
  }

  String _formatSeverity(Level level) {
    if (_colorized) return '';

    if (level < Level.INFO) {
      return 'D ';
    } else if (level < Level.WARNING) {
      return 'I ';
    } else if (level < Level.SEVERE) {
      return 'W ';
    } else if (level < Level.SHOUT) {
      return 'E ';
    } else {
      return 'HANG ';
    }
  }
}

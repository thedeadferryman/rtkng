/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'package:args/args.dart';

class InvalidArgError extends ArgParserException {
  static String _formatError(String arg, String message) =>
      "Invalid argument for '$arg': $message";

  InvalidArgError(String arg, String message)
      : super(_formatError(arg, message));
}

abstract class ArgHelper {
  static T assertType<T>(dynamic value, {String name = ''}) => (value is T)
      ? value
      : throw InvalidArgError(
    name,
    "expected '$T', found '${value.runtimeType}'",
  );

  static int parseInt(dynamic value, {String name = ''}) =>
      int.tryParse(assertType(value)) ??
          (throw InvalidArgError(
            name,
            "'$value' is not a valid integer",
          ));
}

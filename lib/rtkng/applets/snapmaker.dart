/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:async';

import 'package:args/args.dart';
import 'package:rtkng/dartimage/dartimage.dart';
import 'package:rtkng/rtkng/core.dart';
import 'package:rtkng/util/duration.dart';

import '../cli.dart';
import 'common/constants.dart';
import 'common/error_handler.dart';
import 'common/snapshot_collector.dart';

class SnapmakerArgs {
  static const defaults = SnapmakerArgs(input: '');

  final String input;
  final String output;
  final Duration time;
  final bool markTimestamp;
  final FontTemplate font;

  const SnapmakerArgs({
    required this.input,
    this.output = './frame.png',
    this.time = Duration.zero,
    this.markTimestamp = true,
    this.font = defaultFont,
  });

  @override
  String toString() => ({
        'input': input,
        'output': output,
        'time': time.toTimecodeString(),
        'markTimestamp': markTimestamp,
        'font': font,
      }).toString();
}

class SnapmakerApplet extends Applet<String, SnapmakerArgs> {
  static const appletInfo = AppletInfo(
    key: 'snapmaker',
    name: 'Snapmaker',
    description: 'Extracts precise-time snapshot from video',
  );

  // region ArgParser
  @override
  final ArgParser argParser = ArgParser()
    ..addOption(
      'input',
      abbr: 'i',
      help: 'File to take snapshot from',
      valueHelp: 'PATH',
      mandatory: true,
    )
    ..addOption(
      'output',
      abbr: 'o',
      help: 'A path to the resulting snapshot (in PNG format)',
      valueHelp: 'PATH',
      defaultsTo: SnapmakerArgs.defaults.output,
    )
    ..addOption(
      'time',
      abbr: 't',
      help:
          'Time to extract snapshot frame at (in seconds). Can be formatted as <h>:<m>:<s>.',
      valueHelp: 'TIME',
      defaultsTo: SnapmakerArgs.defaults.time.toTimecodeString(),
    )
    ..addFlag(
      'timestamps',
      aliases: ['mark-time', 'stamp-time'],
      abbr: 's',
      defaultsTo: SnapmakerArgs.defaults.markTimestamp,
    )
    ..addOption(
      'font-family',
      abbr: 'f',
      help: 'A font name to use for timestamp.',
      valueHelp: 'NAME',
      defaultsTo: SnapmakerArgs.defaults.font.face,
    )
    ..addOption(
      'font-slant',
      help: 'Slant for a font to use for timestamp.',
      valueHelp: 'SLANT',
      defaultsTo: FontSlant.name(SnapmakerArgs.defaults.font.slant),
      allowed: FontSlant.allowed,
    )
    ..addOption(
      'font-weight',
      help: 'Weight for a font to use for timestamp.',
      valueHelp: 'WEIGHT',
      defaultsTo: FontWeight.name(SnapmakerArgs.defaults.font.weight),
      allowed: FontWeight.allowed,
    );

  // endregion

  ErrorHandler get _errorHandler => ErrorHandler(this);

  SnapmakerApplet(AppContext context) : super(context);

  @override
  Future<String> execute(SnapmakerArgs args) async {
    var collector = SnapshotCollector(context, args.input);

    var res = await collector
        .takePrecise(args.time)
        .onError(_errorHandler.handleFutureError);

    var image = await _postprocSnapshot(args, res)
        .onError(_errorHandler.handleFutureError);

    try {
      image.saveAsPng(args.output);
    } catch (e, stackTrace) {
      _errorHandler.handleError(e, stackTrace);
    } finally {
      image.dispose();
    }

    return args.output;
  }

  Future<RasterImage> _postprocSnapshot(
    SnapmakerArgs args,
    SnapshotResponseItem res,
  ) async {
    if (args.markTimestamp) {
      var img = SnapshotTimestamper.stampSingle(res, fontTemplate: args.font);
      res.image.dispose();
      return img;
    } else {
      return res.image;
    }
  }

  @override
  AppletInfo get info => appletInfo;

  @override
  SnapmakerArgs parseArgs(ArgResults args) {
    String input = ArgHelper.assertType(args['input'], name: 'input');
    String output = ArgHelper.assertType(args['output'], name: 'output');

    var fontSlant = FontSlant.fromString(
        ArgHelper.assertType(args['font-slant'], name: 'font-slant'));

    var fontWeight = FontWeight.fromString(
        ArgHelper.assertType(args['font-weight'], name: 'font-weight'));

    var font = FontTemplate(
      ArgHelper.assertType(args['font-family']),
      fontSlant ?? FontSlant.normal,
      fontWeight ?? FontWeight.normal,
    );

    Duration time;

    try {
      time = parseDuration(ArgHelper.assertType(args['time']));
    } on InvalidArgError {
      rethrow;
    } on FormatException catch (e) {
      throw InvalidArgError('time', e.message);
    } on ArgumentError catch (e) {
      throw InvalidArgError('time', e.toString());
    }

    return SnapmakerArgs(
      input: input,
      output: output,
      font: font,
      markTimestamp: ArgHelper.assertType(args['timestamps']),
      time: time,
    );
  }
}

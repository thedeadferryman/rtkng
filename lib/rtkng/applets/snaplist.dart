/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:async';
import 'dart:io';

import 'package:args/args.dart';
import 'package:path/path.dart' as p;
import 'package:rtkng/dartavconv/media_info.dart';
import 'package:rtkng/dartimage/dartimage.dart';
import 'package:rtkng/progressbar/progressbar.dart';
import 'package:rtkng/rtkng/cli.dart';
import 'package:rtkng/rtkng/core.dart';
import 'package:rtkng/util/frac.dart';
import 'package:rtkng/util/lazy.dart';

import 'common/collage_composer.dart';
import 'common/constants.dart';
import 'common/error_handler.dart';
import 'common/header_drawer.dart';
import 'common/info_extractor.dart';
import 'common/snapshot_collector.dart';

class SnaplistArgs {
  static const defaults = SnaplistArgs(input: '');

  final String input;
  final String output;
  final int frameCount;
  final int maxWidth;
  final Frac infoRatio;
  final FontTemplate font;

  const SnaplistArgs({
    required this.input,
    this.output = './preview.png',
    this.frameCount = 24,
    this.maxWidth = 1280,
    this.infoRatio = const Frac.fromInt(10),
    this.font = defaultFont,
  });

  @override
  String toString() => ({
        'input': input,
        'output': output,
        'frameCount': frameCount,
        'maxWidth': maxWidth,
        'infoRatio': infoRatio,
        'font': font,
      }).toString();
}

class SnaplistApplet extends Applet<String, SnaplistArgs> {
  static const appletInfo = AppletInfo(
    key: 'snaplist',
    name: 'SnapList',
    description: 'Draws a collage with video snapshots '
        'and brief information about the video',
  );

  // region ArgParser
  @override
  final ArgParser argParser = ArgParser()
    ..addOption(
      'input',
      abbr: 'i',
      help: 'A file to process',
      valueHelp: 'PATH',
      mandatory: true,
    )
    ..addOption(
      'output',
      abbr: 'o',
      help: 'A path to resulting snaplist (in PNG format)',
      valueHelp: 'PATH',
      defaultsTo: SnaplistArgs.defaults.output,
    )
    ..addOption(
      'frame-count',
      abbr: 'c',
      help: 'Count of frames to place on the snaplist',
      valueHelp: 'NUM',
      defaultsTo: SnaplistArgs.defaults.frameCount.toString(),
    )
    ..addOption(
      'max-width',
      abbr: 'w',
      help: 'Maximal width for the snaplist',
      valueHelp: 'NUM',
      defaultsTo: SnaplistArgs.defaults.maxWidth.toString(),
    )
    ..addOption(
      'info-ratio',
      help: "A 'width/height' ratio for an information header. "
          "Should be either an integer or a fraction in 'a/b' format.",
      valueHelp: 'FRAC',
      defaultsTo: SnaplistArgs.defaults.infoRatio.toString(),
    )
    ..addOption(
      'font-family',
      abbr: 'f',
      help: 'A font name to use for information output',
      valueHelp: 'NAME',
      defaultsTo: SnaplistArgs.defaults.font.face,
    )
    ..addOption(
      'font-slant',
      help: 'Slant for a font to use for information output',
      valueHelp: 'SLANT',
      defaultsTo: FontSlant.name(SnaplistArgs.defaults.font.slant),
      allowed: FontSlant.allowed,
    )
    ..addOption(
      'font-weight',
      help: 'Weight for a font to use for information output',
      valueHelp: 'WEIGHT',
      defaultsTo: FontWeight.name(SnaplistArgs.defaults.font.weight),
      allowed: FontWeight.allowed,
    );

  // endregion

  @override
  AppletInfo get info => appletInfo;

  ErrorHandler get _errorHandler => ErrorHandler(this);

  SnaplistApplet(AppContext context) : super(context);

  @override
  void init() {
    logger.fine('Initializing libdartimage...');
    DartImageLibrary.initialize();
    logger.fine('libdartimage initialized');
  }

  @override
  SnaplistArgs parseArgs(ArgResults args) {
    String input = ArgHelper.assertType(args['input'], name: 'input');
    String output = ArgHelper.assertType(args['output'], name: 'output');

    var frameCount =
        ArgHelper.parseInt(args['frame-count'], name: 'frame-count');

    var maxWidth = ArgHelper.parseInt(args['max-width'], name: 'max-width');

    var infoRatio = Frac.parse(
        ArgHelper.assertType(args['info-ratio'], name: 'info-ratio'));

    var fontSlant = FontSlant.fromString(
        ArgHelper.assertType(args['font-slant'], name: 'font-slant'));

    var fontWeight = FontWeight.fromString(
        ArgHelper.assertType(args['font-weight'], name: 'font-weight'));

    var font = FontTemplate(
      ArgHelper.assertType(args['font-family']),
      fontSlant ?? FontSlant.normal,
      fontWeight ?? FontWeight.normal,
    );

    return SnaplistArgs(
      input: input,
      output: output,
      frameCount: frameCount,
      maxWidth: maxWidth,
      infoRatio: infoRatio,
      font: font,
    );
  }

  @override
  Future<String> execute(SnaplistArgs args) async {
    logger.finer('Running ${info.key} with args: $args');

    logger.info('Extracting video information');

    var mediaInfo = await InfoExtractor(context)
        .extract(args.input)
        .onError(_errorHandler.handleFutureError);

    logger.finer('Extracted info: $mediaInfo');

    var resolution =
        mediaInfo.video.isNotEmpty ? mediaInfo.video.first.resolution : null;

    if (resolution == null) {
      logger.severe('Failed to determine input file resolution');
      throw AppletFailure(info, -1);
    }

    var shotResponse = await _extractShots(args);

    logger.finer('Scheduling timecode stamps');

    var shots = await _stampShots(shotResponse, args)
        .onError(_errorHandler.handleFutureError);

    var collage = await _composeCollage(shots, resolution, args) //
        .onError(ErrorHandler.rethrowAfter((_, __) => shotResponse.dispose()))
        .onError(_errorHandler.handleFutureError);

    logger.finer('Removing assigned tempdir');

    shotResponse.dispose().then((value) => logger.finer('Tempdir removed'));

    logger.info('Drawing info header');

    var header = await _drawHeader(mediaInfo, args, collage)
        .onError(ErrorHandler.rethrowAfter((_, __) => collage.dispose()))
        .onError(_errorHandler.handleFutureError);

    logger.finer('Drawn header of size ${header.width}x${header.height}');

    logger.info('Building output image');

    RasterImage canvas = await _composeScreenlist(header, collage)
        .onError(ErrorHandler.rethrowAfter((_, __) => header.dispose()))
        .onError(ErrorHandler.rethrowAfter((_, __) => collage.dispose()))
        .onError(_errorHandler.handleFutureError);

    logger.finer('Built output of size ${header.width}x${header.height}');

    logger.info('Writing out');

    header.dispose();
    collage.dispose();

    try {
      canvas.saveAsPng(args.output);
    } catch (e, stackTrace) {
      canvas.dispose();

      logger.severe('Failed to write out ${args.output}');

      _errorHandler.handleError(e, stackTrace);
    }

    logger.finer(
        'Written out ${args.output} of ${await File(args.output).length()} bytes');

    logger.finer('Cleaning out');

    canvas.dispose();

    logger.info('Done processing ${args.output}');

    return args.output;
  }

  Future<List<Lazy<RasterImage>>> _stampShots(
    SnapshotResponse shotResponse,
    SnaplistArgs args,
  ) async =>
      SnapshotTimestamper.stampAll(
        shotResponse,
        fontTemplate: args.font,
      );

  Future<RasterImage> _composeScreenlist(
      RasterImage header, RasterImage collage) async {
    var canvas = RasterImage(
        header.width.ceil(), (header.height + collage.height).ceil());

    canvas.drawImage(header, 0, 0);

    canvas.drawImage(collage, 0, header.height);

    return canvas;
  }

  Future<RasterImage> _drawHeader(
      MediaInfo mediaInfo, SnaplistArgs args, RasterImage collage) async {
    var drawer = InfoHeaderDrawer(context);

    var header = drawer.draw(mediaInfo, args.font, collage.width.ceil(),
        (args.infoRatio.invert() * Frac.fromInt(collage.width.ceil())).ceil());
    return header;
  }

  Future<RasterImage> _composeCollage(List<Lazy<RasterImage>> shots,
      ImageResolution resolution, SnaplistArgs args) async {
    var composer = CollageComposer(shots, resolution, maxWidth: args.maxWidth);

    var collage = await _progressCompose(shots.length)
        .run((pb) => composer.compose(onImageDrawed: (img, cur, total) {
              img.dispose();

              pb.current = cur;
              pb.total = total;
            }));

    return collage;
  }

  Future<SnapshotResponse> _extractShots(SnaplistArgs args) async {
    var scraper = SnapshotCollector(context, args.input);

    var shotResponse = await _progressExtract(
      args.frameCount,
      p.basename(args.input),
    ).run((pb) {
      return scraper.collect(args.frameCount, (cur, total) {
        pb.current = cur;
        pb.total = total;
      }).onError(_errorHandler.handleFutureError);
    });

    return shotResponse;
  }

  ProgressBar _progressExtract(int total, String filename) =>
      createProgressBar(total: total, caption: 'Snapping $filename');

  ProgressBar _progressCompose(int total) =>
      createProgressBar(total: total, caption: 'Composing snaplist');
}

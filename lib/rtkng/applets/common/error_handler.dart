/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import 'package:rtkng/rtkng/core.dart';
import 'package:rtkng/util/sys/ffi.dart';

class ErrorHandler {
  final Applet _applet;

  static Future<T> Function(Object error, StackTrace stackTrace)
  rethrowAfter<T>(
      void Function(Object error, StackTrace stackTrace) handler) {
    return (error, stackTrace) {
      handler(error, stackTrace);

      return Future.error(error, stackTrace);
    };
  }

  ErrorHandler(this._applet);

  Never handleError(Object error, StackTrace stackTrace) =>
      throw _handleAnyError(error, stackTrace);

  Future<T> handleFutureError<T>(Object error, StackTrace stackTrace) =>
      Future.error(_handleAnyError(error, stackTrace));

  AppletFailure _handleAnyError(Object error, StackTrace stackTrace) {
    if (error is FFIError) {
      return _handleFFIError(error, stackTrace);
    } else {
      _applet.logger.severe('Unexpected error', error, stackTrace);
      return AppletFailure(_applet.info, 1, error, stackTrace);
    }
  }

  AppletFailure _handleFFIError(FFIError error, StackTrace stackTrace) {
    _applet.logger.severe('${error.runtimeType}: ${error.message}', error, stackTrace);
    return AppletFailure(_applet.info,
        -(error.code << error.codeOffset), error, stackTrace);
  }
}

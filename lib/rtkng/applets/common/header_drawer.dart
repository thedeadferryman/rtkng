/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:math';

import 'package:rtkng/dartavconv/media_info.dart';
import 'package:rtkng/dartimage/dartimage.dart';
import 'package:rtkng/rtkng/core.dart';
import 'package:rtkng/util/filesize.dart';

abstract class CaptionComposer {
  static List<String> compose(MediaInfo info) {
    return [
          'Filename: ${info.container.filename}',
          _generateContainerLine(info.container),
          _generateVideoLine(info.video),
        ] +
        _generateAudioLines(info.audio);
  }

  static String _generateContainerLine(ContainerInfo container) {
    return 'Container: [${humanizeBytesize(container.size!)}; '
        '${container.duration}] ${container.format}';
  }

  static String _generateVideoLine(Multiplex<VideoInfo> video) {
    return 'Video: ${video[0].resolution}; FPS ${(video[0].frameRate ?? 0).toStringAsFixed(2)}; '
        '${video[0].codec} [${video[0].pixelFormat}]';
  }

  static List<String> _generateAudioLines(Multiplex<AudioInfo> audio) {
    if (audio.length == 1) {
      return [
        'Audio: ${audio[0].codec}; '
            '${(audio[0].bitrate / 1024.0).toStringAsFixed(2)} Kbps'
      ];
    }

    return audio
        .asMap()
        .entries
        .map((e) => 'Audio #${e.key}: ${e.value.codec}; '
            '${(e.value.bitrate / 1024.0).toStringAsFixed(2)} Kbps')
        .toList();
  }
}

class InfoHeaderDrawer {
  static const _fontSizeCoef = 5.5;
  static const _paddingCoef = 5.2;
  static const _gapCoef = 15.5;

  static const _bgColor = Color.white;
  static const _textColor = Color.black;

  final AppContext _context;

  ResourceService get _res => ResourceService(_context);

  InfoHeaderDrawer(this._context);

  RasterImage draw(MediaInfo info, FontTemplate font, int width, int height) {
    var fontSize = height / _fontSizeCoef;
    var padding = height / _paddingCoef;
    var captionGap = height / _gapCoef;

    var captionText = CaptionComposer.compose(info);

    var caption = _drawMediaInfo(
        captionText, font.withSize(fontSize), _textColor, captionGap.ceil());

    var actualHeight = (caption.height + padding * 2).ceil();
    var actualWidth = width;

    var canvas = RasterImage(actualWidth, actualHeight);

    canvas.fill(_bgColor);

    canvas.drawImage(caption, padding, padding);

    caption.dispose();

    var logo = SVGImage(_res.getResourcePath(ResourceType.image, 'logo.svg'));

    var ratio = actualHeight / logo.height;
    var logoWidth = logo.width * ratio;

    var logoShift = actualWidth - logoWidth;

    canvas.drawSVG(logo, logoShift, 0, ratio);

    logo.dispose();

    var resultHeight = ((actualWidth * actualHeight) / width);

    var result = canvas.resize(width.toDouble(), resultHeight);

    canvas.dispose();

    return result;
  }

  RasterImage _drawMediaInfo(
      Iterable<String> lines, Font font, Color color, int hGap) {
    var extents = lines.map((line) => RasterImage.getTextExtents(line, font));

    var width = extents.map((e) => e.width).reduce(max);
    var height = extents.map((e) => e.height).reduce((v, e) => v + e.ceil()) +
        hGap * (lines.length - 1);

    var canvas = RasterImage(width.ceil(), height.ceil(), PixelFormat.rgba);

    var hOffset = 0.0;

    for (var line in lines) {
      var ex = RasterImage.getTextExtents(line, font);

      canvas.drawText(line, font, color, 0, hOffset.ceil());

      hOffset += hGap + ex.height;
    }

    return canvas;
  }
}

/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:math';

import 'package:rtkng/dartavconv/media_info.dart';
import 'package:rtkng/dartimage/dartimage.dart';
import 'package:rtkng/util/collections.dart';
import 'package:rtkng/util/lazy.dart';

class _PositionedImage {
  final Lazy<RasterImage> image;
  final Point<int> position;

  _PositionedImage(this.image, this.position);
}

class CollageComposerException implements Exception {}

dynamic _noop3Args(dynamic arg1, dynamic arg2, dynamic arg3) => null;

class CollageComposer {
  final Iterable<Lazy<RasterImage>> _images;
  final ImageResolution _resolution;
  final int _maxWidth;
  final int _perRow;
  final Color _color;

  CollageComposer(this._images, this._resolution,
      {int perRow = 4, int maxWidth = 1280, Color color = Color.white})
      : _perRow = perRow,
        _maxWidth = maxWidth,
        _color = color;

  RasterImage compose(
      {void Function(RasterImage, int, int) onImageDrawed = _noop3Args}) {
    var layout = _alignLayout(_images.splitBatches(_perRow).toListMatrix());

    var collage = _drawLayout(layout, onImageDrawed);

    return collage;
  }

  double get _origTileWidth => _resolution.width.toDouble();

  double get _origTileHeight => _resolution.height.toDouble();

  int get _tileWidth => (_origTileWidth * _ratio).ceil();

  int get _tileHeight => (_origTileHeight * _ratio).ceil();

  int get _perCol => (_images.length / _perRow).ceil();

  double get _origCollageWidth => _origTileWidth * _perRow;

  double get _origCollageHeight => _origTileHeight * _perCol;

  int get _collageWidth => (_origCollageWidth * _ratio).ceil();

  int get _collageHeight => (_origCollageHeight * _ratio).ceil();

  double get _ratio => min(_maxWidth, _origCollageWidth) / _origCollageWidth;

  List<_PositionedImage> _alignLayout(List<List<Lazy<RasterImage>>> layout) {
    var _images = <_PositionedImage>[];

    for (var row = 0; row < layout.length; row++) {
      for (var col = 0; col < layout[row].length; col++) {
        var image = layout[row][col];

        var colShift = _tileHeight * row;
        var rowShift = _tileWidth * col;

        _images += [_PositionedImage(image, Point(rowShift, colShift))];
      }
    }

    return _images;
  }

  RasterImage _drawLayout(List<_PositionedImage> layout,
      void Function(RasterImage, int, int) onImageDrawed) {
    var canvas = RasterImage(_collageWidth, _collageHeight);

    canvas.fill(_color);

    var processed = 0;

    for (var item in layout) {
      var img = item.image.resolve();

      canvas.drawImage(
          img, item.position.x.toDouble(), item.position.y.toDouble(),
          scale: _ratio);

      onImageDrawed(img, ++processed, layout.length);
    }

    return canvas;
  }
}

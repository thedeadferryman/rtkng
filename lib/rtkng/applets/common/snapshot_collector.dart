/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'package:collection/collection.dart';
import 'package:rtkng/dartavconv/dartavconv.dart';
import 'package:rtkng/dartimage/dartimage.dart';
import 'package:rtkng/rtkng/core.dart';
import 'package:rtkng/util/duration.dart';
import 'package:rtkng/util/lazy.dart';
import 'package:rtkng/util/sys/tempfile.dart';

class SnapshotResponseItem {
  final RasterImage image;
  final Duration timestamp;

  SnapshotResponseItem(this.image, this.timestamp);
}

class SnapshotResponse extends DelegatingList<Lazy<SnapshotResponseItem>> {
  final Future<void> Function() _dispose;

  SnapshotResponse(List<Lazy<SnapshotResponseItem>> base, this._dispose)
      : super(base);

  Future<void> dispose() => _dispose();
}

void _noop2Args(a, b) {}

class SnapshotCollector {
  final AppContext _ctx; // ignore: unused_field

  final String _inPath;

  SnapshotCollector(this._ctx, this._inPath);

  Future<SnapshotResponse> collect(int count,
      [void Function(int current, int total) onProgress = _noop2Args]) async {
    var tempDir =
        await TempDir.create(parent: _ctx.tempDir, prefix: 'rtkng-snaplist-');

    try {
      var result = InternalSnapshotCollector.withOnProgress(
        onProgress,
        () => InternalSnapshotCollector()
            .collect(
              _inPath,
              tempDir.toString(),
              count,
            )
            .map((e) => Lazy(() => SnapshotResponseItem(
                  RasterImage.fromPNG(e.outPath),
                  Duration(seconds: e.timecode),
                )))
            .toList(),
      );

      return SnapshotResponse(result, () => tempDir.dispose());
    } catch (e) {
      tempDir.dispose();
      rethrow;
    }
  }

  Future<SnapshotResponseItem> takePrecise(Duration time) async {
    var tempDir = await TempDir.create(prefix: 'rtkng-snapmaker-');

    var res = InternalSnapshotCollector().takePrecise(
      _inPath,
      tempDir.assignRandomTempFile('png'),
      time.inSeconds,
    );

    var img = RasterImage.fromPNG(res.outPath);

    await tempDir.dispose();

    return SnapshotResponseItem(img, Duration(seconds: res.timecode));
  }
}

abstract class SnapshotTimestamper {
  static const _paddingCoef = 48;
  static const _fontSizeCoef = 24;
  static const _textColor = Color.white;

  static List<Lazy<RasterImage>> stampAll(
    SnapshotResponse response, {
    FontTemplate? fontTemplate,
    Font? font,
  }) =>
      response
          .thenMap((item) =>
              stampSingle(item, font: font, fontTemplate: fontTemplate))
          .toList();

  static RasterImage stampSingle(
    SnapshotResponseItem item, {
    FontTemplate? fontTemplate,
    Font? font,
  }) {
    if (font != null) {
      return _stampSingleWithFont(item, font);
    } else if (fontTemplate != null) {
      return _stampSingleWithTemplate(item, fontTemplate);
    } else {
      throw ArgumentError.notNull("'font' or 'fontTemplate'");
    }
  }

  static RasterImage _stampSingleWithTemplate(
    SnapshotResponseItem response,
    FontTemplate fontTemplate,
  ) {
    var font = fontTemplate.withSize(response.image.width / _fontSizeCoef);

    return _stampSingleWithFont(response, font);
  }

  static RasterImage _stampSingleWithFont(
    SnapshotResponseItem response,
    Font font,
  ) {
    var padX = (response.image.width / _paddingCoef).ceil();
    var padY = (response.image.height / _paddingCoef).ceil();

    response.image.drawText(
        response.timestamp.toTimecodeString(), font, _textColor, padX, padY,
        outlined: true);

    return response.image;
  }
}

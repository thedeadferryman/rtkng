/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:async';

import 'package:args/args.dart';
import 'package:rtkng/rtkng/core.dart';

enum TernorType { photoset, single, pack }

enum FileLinkType { symlink, copy, move }

class TernorArgs {
  final String output;
  final String name;
  final String poster;
  final TernorType type;
  final FileLinkType linkType;
  final List<String> targetFiles;

  TernorArgs({
    required this.output,
    this.name = '',
    this.poster = '',
    this.type = TernorType.single,
    this.linkType = FileLinkType.copy,
    required this.targetFiles,
  });
}

class TernorApplet extends Applet<String, TernorArgs> {
  static const appletInfo = AppletInfo(
    key: 'ternor',
    name: 'Ternor',
    description: 'Creates a template for a video release, '
        'containing a directory with video files and a config file.',
  );

  // region ArgParser
  // endregion

  TernorApplet(AppContext context) : super(context);

  @override
  Future<String> execute(TernorArgs args) {
    // TODO: implement execute
    throw UnimplementedError();
  }

  @override
  // TODO: implement info
  AppletInfo get info => appletInfo;

  @override
  FutureOr<TernorArgs> parseArgs(ArgResults args) {
    // TODO: implement parseArgs
    throw UnimplementedError();
  }
}

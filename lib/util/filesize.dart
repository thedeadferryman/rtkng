/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// Ported from `package:filesize`
String humanizeBytesize(int size, [int round = 2]) {
  const divider = 1024;

  if (size < divider) {
    return '$size B';
  }

  if (size < divider * divider && size % divider == 0) {
    return '${(size / divider).toStringAsFixed(0)} KB';
  }

  if (size < divider * divider) {
    return '${(size / divider).toStringAsFixed(round)} KB';
  }

  if (size < divider * divider * divider && size % divider == 0) {
    return '${(size / (divider * divider)).toStringAsFixed(0)} MB';
  }

  if (size < divider * divider * divider) {
    return '${(size / divider / divider).toStringAsFixed(round)} MB';
  }

  if (size < divider * divider * divider * divider && size % divider == 0) {
    return '${(size / (divider * divider * divider)).toStringAsFixed(0)} GB';
  }

  if (size < divider * divider * divider * divider) {
    return '${(size / divider / divider / divider).toStringAsFixed(round)} GB';
  }

  if (size < divider * divider * divider * divider * divider &&
      size % divider == 0) {
    num r = size / divider / divider / divider / divider;
    return '${r.toStringAsFixed(0)} TB';
  }

  if (size < divider * divider * divider * divider * divider) {
    num r = size / divider / divider / divider / divider;
    return '${r.toStringAsFixed(round)} TB';
  }

  if (size < divider * divider * divider * divider * divider * divider &&
      size % divider == 0) {
    num r = size / divider / divider / divider / divider / divider;
    return '${r.toStringAsFixed(0)} PB';
  } else {
    num r = size / divider / divider / divider / divider / divider;
    return '${r.toStringAsFixed(round)} PB';
  }
}
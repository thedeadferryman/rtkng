/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

extension PrintTimecode on Duration {
  String toTimecodeString({bool detailed = false}) =>
      [inHours, _onlyMinutes, _onlySeconds]
          .map((e) => e.toString())
          .map((e) => e.padLeft(2, '0'))
          .join(':') +
      (detailed ? '.' + _onlyMillis.toString().padLeft(3, '0') : '');

  int get _onlyMinutes => inMinutes - (inHours * Duration.minutesPerHour);

  int get _onlySeconds => inSeconds - (inMinutes * Duration.secondsPerMinute);

  int get _onlyMillis =>
      inMilliseconds - (inSeconds * Duration.millisecondsPerSecond);
}

Duration parseDuration(String duration) {
  var parts = duration.trim().split(':').reversed.toList(growable: false);

  if (parts.length > 3 || parts.isEmpty) {
    throw ArgumentError.value(duration, 'duration', 'Invalid Duration string');
  }

  var seconds = int.parse(parts[0], radix: 10);
  int? minutes;
  int? hours;

  if (parts.length >= 2) {
    minutes = int.parse(parts[1], radix: 10);
  }

  if (parts.length >= 3) {
    hours = int.parse(parts[2], radix: 10);
  }

  return Duration(
    hours: (hours ?? 0),
    minutes: (minutes ?? 0),
    seconds: seconds,
  );
}

/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Lazy<T> {
  final T Function() _factory;

  Lazy(this._factory);

  T resolve() => _factory();

  Lazy<T2> then<T2>(T2 Function(T) applier) => Lazy(() => applier(_factory()));
}

extension LazyIterable<T> on Iterable<Lazy<T>> {
  Iterable<T> resolve() => map((e) {
        return e.resolve();
      });

  Iterable<Lazy<T2>> thenMap<T2>(T2 Function(T) mapper) =>
      map((e) => e.then(mapper));
}

/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:async';
import 'dart:math';

typedef BatchExecutorJob<R> = Future<R> Function();
typedef Finalizer = void Function();

class BatchExecutor<R> {
  List<BatchExecutorJob<R>> _jobs;
  final int _limitParallel;
  var _runningJobs = 0;
  final streamController = StreamController<R>();
  final Finalizer _finalizer;

  BatchExecutor(this._jobs, {int? limit, Finalizer? finalizer})
      : _limitParallel = limit ?? _jobs.length,
        _finalizer = finalizer ?? (() {});

  int get _jobCount => min(_limitParallel - _runningJobs, _jobs.length);

  Stream<R> execute() {
    _refreshState();

    return streamController.stream;
  }

  void _refreshState() {
    var jobCount = _jobCount;

    if (jobCount <= 0 && _runningJobs <= 0) {
      streamController.close();

      _finalizer();

      return;
    }

    var jobs = _jobs.take(jobCount);
    _jobs = _jobs.skip(jobCount).toList();

    jobs.forEach(_submitJob);
  }

  void _submitJob(BatchExecutorJob<R> job) {
    _runningJobs += 1;

    job().then(_onJobFinished);
  }

  void _onJobFinished(R value) {
    _runningJobs -= 1;

    streamController.add(value);

    _refreshState();
  }
}

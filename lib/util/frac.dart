/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Frac {
  final int numerator;
  final int denominator;

  const Frac(this.numerator, this.denominator);

  const Frac.fromInt(this.numerator) : denominator = 1;

  factory Frac.parse(String str) {
    var parts = str.split('/');

    var denom = 1;

    var num = int.parse(parts.first, radix: 10);

    if (parts.length > 1) {
      denom = int.parse(parts[1], radix: 10);
    }

    return Frac(num, denom);
  }

  Frac operator -() => Frac(-numerator, denominator);

  Frac operator +(Frac other) {
    var gcd = denominator.gcd(other.denominator);

    var denom = (denominator ~/ gcd) * other.denominator;

    var num = (numerator * (other.denominator ~/ gcd)) +
        (other.numerator * (denominator ~/ gcd));

    return Frac(num, denom);
  }

  operator -(Frac other) => this + (-other);

  Frac operator *(Frac other) {
    var num = numerator * other.numerator;
    var denom = denominator * other.denominator;

    var gcd = num.gcd(denom);

    return Frac(
      num ~/ gcd,
      denom ~/ gcd,
    );
  }

  Frac invert() => Frac(denominator, numerator);

  Frac operator /(Frac other) => this * other.invert();

  double toDouble() => numerator / denominator;

  int ceil() => toDouble().ceil();

  int floor() => toDouble().floor();

  @override
  String toString() =>
      denominator != 1 ? '$numerator/$denominator' : numerator.toString();
}

extension IntFracArith on int {
  Frac operator +(Frac frac) => Frac.fromInt(this) + frac;

  Frac operator -(Frac frac) => Frac.fromInt(this) - frac;

  Frac operator *(Frac frac) => Frac.fromInt(this) * frac;

  Frac operator /(Frac frac) => Frac.fromInt(this) / frac;
}

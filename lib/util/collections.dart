/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

extension SplitBatches<T> on Iterable<T> {
  Iterable<Iterable<T>> splitBatches(int fragmentSize) {
    var origin = toList(growable: false);

    var fragments = <Iterable<T>>[];
    var fragment = <T>[];

    for (var i = 0; i < length; i++) {
      if (i > 0 && i % fragmentSize == 0) {
        fragments += [fragment];
        fragment = [];
      }

      fragment += [origin[i]];
    }

    return fragments + [fragment];
  }
}

extension MatrixToList<T> on Iterable<Iterable<T>> {
  List<List<T>> toListMatrix() {
    return map((e) => e.toList()).toList();
  }
}

extension WhereNotNull<T> on Iterable<T?> {
  Iterable<T> whereNotNull() {
    return where((e) => e != null).map((e) => e!);
  }
}


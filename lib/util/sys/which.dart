/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:io';

import 'package:path/path.dart' as p;
import 'package:rtkng/util/collections.dart' show WhereNotNull;

import 'platform.dart';

class ExecutableExtensions {
  static final ExecutableExtensions allVariants = ExecutableExtensions();

  String _applyExtension(String executable, String extension) {
    if (extension.isEmpty) return executable;

    return p.setExtension(executable, extension);
  }

  List<String> call(String executable) {
    return executableExtensions()
        .map((ext) => _applyExtension(executable, ext))
        .toList();
  }
}

class WhichResolveException implements Exception {
  final String command;

  WhichResolveException(this.command);
}

class WhichExecutable {
  List<String> additionalPaths;

  WhichExecutable(this.additionalPaths);

  List<String> callPaths() {
    if (Platform.isLinux) {
      return ((Platform.environment['PATH']?.split(':') ?? <String>[]) +
          additionalPaths);
    } else {
      return ((Platform.environment['PATH']?.split(';') ?? <String>[]) +
          additionalPaths);
    }
  }

  Future<String> resolve(String executable) async {
    var variants = await Future.wait(callPaths()
        .map((path) => _variantsInPath(path, executable))
        .expand((p) => p)
        .map((path) => _normalizePath(path)));

    try {
      return variants.whereNotNull().firstWhere((path) => _isValidPath(path));
    } on StateError {
      throw WhichResolveException(executable);
    }
  }

  List<String> _variantsInPath(String path, String executable) {
    return ExecutableExtensions.allVariants(p.join(path, executable));
  }

  Future<String?> _normalizePath(String path) async {
    var file = File(path);
    var stat = await file.stat();

    if (stat.type == FileSystemEntityType.notFound) return null;

    if (stat.type == FileSystemEntityType.link) {
      return Link(path).resolveSymbolicLinks();
    }

    return p.normalize(path);
  }

  bool _isValidPath(String? path) {
    if (path == null) return false;

    return (File(path).statSync().type == FileSystemEntityType.file);
  }
}

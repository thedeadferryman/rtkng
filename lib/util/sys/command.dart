/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:convert';
import 'dart:io' as io;

import 'package:path/path.dart' as p;

import 'platform.dart';

class Verbosity {
  static const Verbosity basic = Verbosity();
  static const Verbosity silent = Verbosity(
    stdout: false,
    stderr: false,
    exitCode: false,
  );
  static const Verbosity verbose = Verbosity(stdout: true);

  final bool stdout;
  final bool stderr;
  final bool exitCode;

  const Verbosity(
      {this.stdout = false, this.stderr = true, this.exitCode = true});
}

enum StreamType { stdout, stderr }

class ShellException implements Exception {
  int exitCode;

  ShellException(this.exitCode);
}

class ExecutingCommand {
  final Stream<List<int>> stdout;
  final Stream<List<int>> stderr;
  final Stream<int> exitCode;

  final io.Process process;

  ExecutingCommand({
    required this.stdout,
    required this.stderr,
    required this.exitCode,
    required this.process,
  });
}

class ShellCommand {
  final String _executor;
  final List<String> _args;

  ShellCommand(this._executor, this._args);

  @override
  String toString() {
    return '"$_executor" ' + _args.map((e) => '"$e"').join(' ');
  }

  Future<ExecutingCommand> execute(
      {Verbosity verbosity = Verbosity.basic, bool throwOnExitCode = false}) {
    return io.Process.start(_executor, _args, runInShell: true)
        .then((process) async {
      var stderr = process.stderr.asBroadcastStream();
      var stdout = process.stdout.asBroadcastStream();
      var exitCode = process.exitCode.asStream().asBroadcastStream();

      stdout.transform(utf8.decoder).listen((event) {
        if (verbosity.stdout) {
          io.stdout.write(_logProcessOutputLines(process.pid, event, StreamType.stdout));
        }
      });

      stderr.transform(utf8.decoder).listen((event) {
        if (verbosity.stderr) {
          io.stderr.write(_logProcessOutputLines(process.pid, event, StreamType.stderr));
        }
      });

      if (verbosity.exitCode) {
        exitCode.listen((code) {
          io.stderr.writeln(_logProcessExitCode(process.pid, code));
        });
      }

      if (throwOnExitCode) {
        exitCode.listen((code) {
          if (code != 0) throw ShellException(code);
        });
      }

      return ExecutingCommand(
          stdout: stdout, stderr: stderr, exitCode: exitCode, process: process);
    });
  }

  String _logProcessExitCode(int pid, int code) {
    return '[${_processDesc(pid)}] exited with code $code';
  }

  String _logProcessOutputLines(int pid, String data, StreamType type) {
    String streamType;
    switch (type) {
      case StreamType.stderr:
        streamType = 'err';
        break;
      case StreamType.stdout:
        streamType = 'out';
        break;
      default:
        streamType = 'unk';
    }

    return data
        .split(lineSeparator)
        .map((line) => '[${_processDesc(pid)} $streamType] $line')
        .join(lineSeparator);
  }

  String _processDesc(int pid) {
    return '${_executorBasename()}/$pid';
  }

  String _executorBasename() {
    return p.basenameWithoutExtension(_executor);
  }
}

/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:io';

import 'package:path/path.dart' as p;

class TempDirException implements Exception {
  final String path;

  TempDirException(this.path);
}

class TempDir {
  final String _prefix;
  final Directory _tempDir;
  int _postfix = 0;

  TempDir(this._tempDir, this._prefix);

  static Future<TempDir> create({String? parent, String prefix = 'tmp-'}) async {
    var dir = parent != null ? Directory(p.canonicalize(parent)) : Directory.current;

    if (!await dir.exists()) {
      throw TempDirException(dir.path);
    }

    return TempDir((await dir.createTemp(prefix)), prefix);
  }

  Future<File> createTempFile(String ext) =>
      File(assignRandomTempFile(ext)).create(recursive: true);

  String assignRandomTempFile(String ext) {
    var filename = '$_prefix${DateTime.now().millisecondsSinceEpoch}-$_postfix';

    _postfix += 1;

    return assignTempFile(p.setExtension(filename, ext));
  }

  String assignTempFile(String filename) => p.join(_tempDir.path, filename);

  Future<void> dispose() {
    return _tempDir.delete(recursive: true);
  }

  @override
  String toString() => _tempDir.path;
}

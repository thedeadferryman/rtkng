/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'package:rtkng/util/string.dart' show Ellipsize;

import 'style.dart';
import 'util/template_parser.dart';

class RendererError extends Error {
  final String? message;

  RendererError([this.message]);
}

class ProgressBarRenderer {
  final int _windowWidth;
  final Style _style;
  final double _paddingRatio;

  int current;
  int total;

  ProgressBarRenderer(
    this.total,
    this.current,
    int windowWidth,
    double paddingRatio,
    Style style,
  )   : _style = style,
        _paddingRatio = paddingRatio,
        _windowWidth = windowWidth;

  int get _width => (_style.alignment == Alignment.stretch)
      ? (_windowWidth - 1)
      : (_windowWidth * (1 - _paddingRatio)).floor();

  int get _paddingLeft {
    switch (_style.alignment) {
      case Alignment.center:
        return ((_windowWidth - _width) / 2).floor();
      case Alignment.right:
        return _windowWidth - _width;
      default:
        return 0;
    }
  }

  int get _barWidth => _width - _fixedPartsWidth;

  int get _fixedPartsWidth => _fixedParts
      .map(_renderFixedPart)
      .map((e) => e.length)
      .reduce((v, e) => (v + e));

  int get _currentProgressWidth => (_barWidth * (current / total)).ceil();

  int get renderWidth => render().length;

  int get cleanupWidth => _windowWidth - 1;

  String render() => (' ' * _paddingLeft) + _renderBar();

  String _renderBar() => _style.template.map(_renderPart).join('');

  String _renderPart(Qualifier qual) {
    if (_isFixedPart(qual)) {
      return _renderFixedPart(qual);
    } else if (qual is ParameterizedQualifier) {
      return _renderIncrustedProgress(qual);
    } else if (qual is SimpleQualifier) {
      switch (qual.type) {
        case QualifierType.caption:
          return _renderCaption();
        case QualifierType.progress:
          return _renderSimpleProgress();
        case QualifierType.spacer:
          return _renderSpacer();
        default:
          throw RendererError('invalid qual');
      }
    } else {
      throw RendererError('invalid qual');
    }
  }

  String _renderCaption() =>
      _style.renderOptions.caption.ellipsize(_width ~/ 2.5);

  String _renderSpacer() =>
      _style.renderOptions.barBody * (_barWidth - _currentProgressWidth);

  String _renderSimpleProgress() =>
      _style.renderOptions.progressBody * _currentProgressWidth;

  String _renderIncrustedProgress(ParameterizedQualifier qual) {
    var contents = qual.param.map(_renderFixedPart).join('');

    /// We don't need to show incrustation when there's
    ///   not enough space for it.
    /// We also don't want progressbar to blink and lose
    ///   its body once there's enough space only for incrustation.
    if ((contents.length + 2) > _currentProgressWidth) {
      return _renderSimpleProgress();
    }

    var wing = _style.renderOptions.progressBody *
        ((_currentProgressWidth - contents.length) ~/ 2);

    return wing + contents + wing;
  }

  String _renderFixedPart(Qualifier qual) {
    if (qual is RawQualifier) {
      return qual.value;
    } else if (qual is SimpleQualifier) {
      switch (qual.type) {
        case QualifierType.caption:
          return _renderCaption();
        case QualifierType.progress:
        case QualifierType.spacer:
          throw RendererError('dynamic qual not allowed');
        case QualifierType.current:
          return '$current';
        case QualifierType.total:
          return '$total';
        case QualifierType.percentage:
          return _style.renderOptions.percentageRenderer(current / total);
        case QualifierType.fractional:
          return _style.renderOptions.fractionalRenderer(current, total);
        default:
          throw RendererError('qual unknown');
      }
    } else {
      throw RendererError('dynamic qual not allowed');
    }
  }

  bool _isFixedPart(Qualifier qual) {
    if (qual is ParameterizedQualifier) {
      return false;
    } else if (qual is SimpleQualifier) {
      switch (qual.type) {
        case QualifierType.progress:
        case QualifierType.spacer:
          return false;
        default:
          return true;
      }
    } else {
      return true;
    }
  }

  List<Qualifier> get _fixedParts =>
      _style.template.where(_isFixedPart).toList();
}

/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'package:petitparser/petitparser.dart' as gr;

enum QualifierType {
  spacer, // %w
  progress, // %b, %B
  percentage, // %p
  fractional, // %f
  current, // %c
  total, // %t
  caption, // %n
}

class _Lexeme {
  final bool raw;
  final String qualifier;
  final List<_Lexeme> param;

  const _Lexeme(this.qualifier, this.param, [this.raw = false]);

  Map<String, dynamic> toJson() {
    return {'raw': raw, 'qual': qualifier, 'param': param};
  }
}

class TemplateParseError implements Exception {
  final String _message;

  String get message => _message;

  TemplateParseError(this._message);
}

abstract class Qualifier {}

class RawQualifier implements Qualifier {
  final String value;

  const RawQualifier(this.value);

  RawQualifier append(RawQualifier other) =>
      RawQualifier('$value${other.value}');
}

class SimpleQualifier implements Qualifier {
  final QualifierType type;

  const SimpleQualifier(this.type);
}

class ParameterizedQualifier implements Qualifier {
  final QualifierType type;
  final List<Qualifier> param;

  const ParameterizedQualifier(this.type, this.param);
}

class TemplateParser {
  // region Lexer

  gr.Parser? _parserCache;

  _Lexeme _raw(String char) => _Lexeme(char, [], true);

  _Lexeme _simple(String char) => _Lexeme(char, []);

  _Lexeme _param(String char, List<dynamic>? param) {
    var rParam = (param ?? []).whereType<_Lexeme>().toList();

    return _Lexeme(char, rParam);
  }

  gr.Parser get _parser {
    if (_parserCache != null) {
      return _parserCache!;
    }

    var qPerc = gr.char('%');
    var qLBrack = gr.char('{');
    var qRBrack = gr.char('}');

    var qTypeSimple = gr.anyOf('pfctn');
    var qTypeBar = gr.anyOf('wbB');

    var qType = qTypeSimple | qTypeBar;

    var qualSimple = (qPerc & qTypeSimple).map((value) => _simple(value[1]));

    var exprSimple = qualSimple.plus();

    var qParam = (qLBrack & exprSimple & qRBrack).map((value) => value[1]);

    var qual = (qPerc & qType & qParam.optional())
        .map((value) => _param(value[1], value[2]));

    var qualPerc = qPerc & qPerc;
    var qualLBrack = qPerc & qLBrack;
    var qualRBrack = qPerc & qRBrack;

    var qualEscaped =
        (qualPerc | qualLBrack | qualRBrack).map((value) => _raw(value[1]));

    var qualRaw = gr.any().map((value) => _raw(value));

    var expr = (qualEscaped | qual | qualRaw).star();

    return _parserCache = expr;
  }

  // endregion Lexer

  List<Qualifier> parse(String source) =>
      _glueQualifiers(_parseQualifiers(_parseLexemes(source)));

  List<Qualifier> _glueQualifiers(List<Qualifier> qualifiers) =>
      qualifiers.fold(<Qualifier>[], (value, element) {
        if (value.isEmpty) return [element];

        var last = value.last;

        if (last is RawQualifier && element is RawQualifier) {
          return value.take(value.length - 1).toList() + [last.append(element)];
        } else {
          return value + [element];
        }
      });

  List<Qualifier> _parseQualifiers(List<_Lexeme> lexemes) =>
      lexemes.map((lexeme) {
        if (lexeme.raw) return RawQualifier(lexeme.qualifier);

        switch (lexeme.qualifier) {
          case 'w':
            return SimpleQualifier(QualifierType.spacer);
          case 'n':
            return SimpleQualifier(QualifierType.caption);
          case 'b':
            return SimpleQualifier(QualifierType.progress);
          case 'B':
            return ParameterizedQualifier(
                QualifierType.progress, _parseQualifiers(lexeme.param));
          case 'p':
            return SimpleQualifier(QualifierType.percentage);
          case 'f':
            return SimpleQualifier(QualifierType.fractional);
          case 'c':
            return SimpleQualifier(QualifierType.current);
          case 't':
            return SimpleQualifier(QualifierType.total);
          default:
            throw TemplateParseError(
                'unexpected qualifier "${lexeme.qualifier}"');
        }
      }).toList();

  List<_Lexeme> _parseLexemes(String str) {
    var result = _parser.parse(str);

    if (result.isFailure) {
      throw TemplateParseError(result.message);
    } else {
      var ast = result.value;

      if (ast is List) {
        return ast.whereType<_Lexeme>().toList();
      } else {
        throw TemplateParseError('unexpexted parse error');
      }
    }
  }
}

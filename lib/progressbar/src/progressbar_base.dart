/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:async';
import 'dart:io';

import 'package:dart_console/dart_console.dart';
import 'package:file/file.dart';

import 'renderer.dart';
import 'style.dart';

class ProgressBar {
  static int _calcWidth(int resolvedWidth, int? widthHint) => widthHint != null
      ? (resolvedWidth <= 1 ? widthHint : resolvedWidth)
      : resolvedWidth;

  final ProgressBarRenderer _cliRenderer;
  final ProgressBarRenderer _nottyRenderer;

  ProgressBarRenderer get _renderer => _isTTY ? _cliRenderer : _nottyRenderer;

  bool get _isTTY => _outputSink == stderr;

  final IOSink _outputSink;

  int get total => _renderer.total;

  int get current => _renderer.current;

  set current(int current) {
    var oldCurrent = _renderer.current;
    _renderer.current = current;
    if (oldCurrent != current) _update();
  }

  set total(int total) {
    var oldTotal = _renderer.total;
    _renderer.total = total;
    if (oldTotal != total) _update();
  }

  Future<R> run<R>(FutureOr<R> Function(ProgressBar) action) async {
    initialize();

    var res = await action(this);

    finalize();

    return res;
  }

  ProgressBar({
    int total = 100,
    int current = 0,
    int? forceWidth,
    int? noTTYWidth,
    int? widthHint,
    double paddingRatio = 0.3,
    Style style = Style.basic,
    Style? noTTYStyle,
    IOSink? outputSink,
  })  : _outputSink = outputSink ?? stderr,
        _cliRenderer = ProgressBarRenderer(
          total,
          current,
          _calcWidth(forceWidth ?? Console().windowWidth, widthHint),
          paddingRatio,
          style,
        ),
        _nottyRenderer = ProgressBarRenderer(
          total,
          current,
          (noTTYWidth ?? forceWidth ?? widthHint ?? 80),
          paddingRatio = 0,
          noTTYStyle ?? style.toNoTTY(),
        );

  void initialize() => _update();

  void increment() {
    current += 1;
    _update();
  }

  void finalize() {
    if (_isTTY) {
      _update();
      _outputSink.write('\n');
    }
  }

  void _update() {
    if (_isTTY) {
      _outputSink.write('\r');
      _outputSink.write(' ' * _renderer.cleanupWidth);
      _outputSink.write('\r');
      _outputSink.write(_renderer.render());
    } else {
      _outputSink.write(_renderer.render());
      _outputSink.write('\n');
    }
  }
}

/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'package:collection/collection.dart';

import 'util/template_parser.dart';

enum Alignment { left, right, center, stretch }

class Template extends DelegatingList<Qualifier> {
  static const basic = Template.__([
    SimpleQualifier(QualifierType.caption),
    RawQualifier('['),
    SimpleQualifier(QualifierType.progress),
    SimpleQualifier(QualifierType.spacer),
    RawQualifier('] '),
    SimpleQualifier(QualifierType.percentage)
  ]);

  static const notty = Template.__([
    SimpleQualifier(QualifierType.caption),
    RawQualifier(': '),
    SimpleQualifier(QualifierType.fractional),
    RawQualifier(' ['),
    SimpleQualifier(QualifierType.percentage),
    RawQualifier(']'),
  ]);

  const Template.__(List<Qualifier> e) : super(e);

  static Template fromString(String template) =>
      Template.__(TemplateParser().parse(template));
}

// region Renderers

typedef PercentageRenderer = String Function(double ratio);

String _defaultPercentageRenderer(double ratio) =>
    (ratio * 100).toStringAsFixed(1) + '%';

typedef FractionalRenderer = String Function(int progress, int? total);

String _defaultFractionalRenderer(int progress, int? total) =>
    '$progress / ${total ?? '--'}';

// endregion

class RenderOptions {
  static const basic = RenderOptions();

  final String progressBody;
  final String barBody;
  final String caption;
  final PercentageRenderer percentageRenderer;
  final FractionalRenderer fractionalRenderer;

  const RenderOptions({
    this.progressBody = '=',
    this.barBody = ' ',
    this.caption = '',
    this.percentageRenderer = _defaultPercentageRenderer,
    this.fractionalRenderer = _defaultFractionalRenderer,
  });

  RenderOptions copy({
    String? progressBody,
    String? barBody,
    String? caption,
    PercentageRenderer? percentageRenderer,
    FractionalRenderer? fractionalRenderer,
  }) =>
      RenderOptions(
        progressBody: progressBody ?? this.progressBody,
        barBody: barBody ?? this.barBody,
        caption: caption ?? this.caption,
        percentageRenderer: percentageRenderer ?? this.percentageRenderer,
        fractionalRenderer: fractionalRenderer ?? this.fractionalRenderer,
      );
}

class Style {
  static const basic = Style();
  static const notty = Style(
    template: Template.notty,
  );

  final Template template;
  final Alignment alignment;
  final RenderOptions renderOptions;

  const Style({
    this.template = Template.basic,
    this.alignment = Alignment.left,
    this.renderOptions = RenderOptions.basic,
  });

  Style toNoTTY() => copy(
        template: Template.notty,
      );

  Style copy({
    Template? template,
    Alignment? alignment,
    RenderOptions? renderOptions,
  }) =>
      Style(
        template: template ?? this.template,
        alignment: alignment ?? this.alignment,
        renderOptions: renderOptions ?? this.renderOptions,
      );
}

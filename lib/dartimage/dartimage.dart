/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:ffi';

import 'package:ffi/ffi.dart';
import 'package:rtkng/util/sys/ffi.dart';

import 'bindings.dart';

abstract class DartImageLibrary {
  static const dylibName = 'libdartimage';

  static DartImage? _lib;

  static void initialize() {
    if (_lib != null) return;

    var dylib = DynamicLibrary.open(dylibName);
    _lib = DartImage(dylib);

    _lib!.DartImage_init();
  }

  static DartImage get instance {
    initialize();

    return _lib!;
  }
}

class _CDEFObject {
  static DartImage get _libStatic => DartImageLibrary.instance;

  DartImage get _lib => _libStatic;
}

class Color {
  static const black = Color.grayscale(0);
  static const white = Color.grayscale(255);

  final int red;
  final int green;
  final int blue;
  final double alpha;

  const Color(this.red, this.green, this.blue, [this.alpha = 1]);

  const Color.grayscale(int shade, [double alpha = 1])
      : this(shade, shade, shade, alpha);
}

abstract class PixelFormat {
  static const int rgb = CairoImage_PixFormat.FORMAT_RGB24;
  static const int rgba = CairoImage_PixFormat.FORMAT_ARGB32;
}

abstract class FontSlant {
  static const normal = FontUtils_FontSlant.SLANT_NORMAL;
  static const italic = FontUtils_FontSlant.SLANT_ITALIC;
  static const oblique = FontUtils_FontSlant.SLANT_OBLIQUE;

  static Iterable<String> allowed = [
    'normal',
    'regular',
    'italic',
    'cursive',
    'oblique'
  ];

  static int? fromString(String str) {
    switch (str.trim().toLowerCase()) {
      case 'normal':
      case 'regular':
        return normal;
      case 'italic':
      case 'cursive':
        return italic;
      case 'oblique':
        return oblique;
      default:
        return null;
    }
  }

  static String name(int slant) {
    switch (slant) {
      case FontSlant.normal:
        return 'regular';
      case FontSlant.italic:
        return 'italic';
      case FontSlant.oblique:
        return 'oblique';
      default:
        return 'invalid';
    }
  }
}

abstract class FontWeight {
  static const normal = FontUtils_FontWeight.WEIGHT_NORMAL;
  static const bold = FontUtils_FontWeight.WEIGHT_BOLD;

  static Iterable<String> allowed = ['normal', 'regular', 'bold'];

  static int? fromString(String str) {
    switch (str.trim().toLowerCase()) {
      case 'normal':
      case 'regular':
        return normal;
      case 'bold':
        return bold;
      default:
        return null;
    }
  }

  static String name(int weight) {
    switch (weight) {
      case FontWeight.normal:
        return 'regular';
      case FontWeight.bold:
        return 'bold';
      default:
        return 'invalid';
    }
  }
}

class Font {
  final String face;
  final double size;
  final int slant;
  final int weight;

  Font(this.face, this.size, this.slant, this.weight);
}

class FontTemplate {
  final String face;
  final int slant;
  final int weight;

  const FontTemplate(this.face,
      [this.slant = FontSlant.normal, this.weight = FontWeight.normal]);

  Font withSize(double size) {
    return Font(face, size, slant, weight);
  }

  @override
  String toString() =>
      '$face S:${FontSlant.name(slant)} W:${FontWeight.name(weight)}';
}

class SVGImage extends _CDEFObject {
  final Pointer<Void> _ptr;

  int get width => _lib.SVGImage_getWidth(_ptr);

  int get height => _lib.SVGImage_getHeight(_ptr);

  SVGImage(String path)
      : _ptr = _CDEFObject._libStatic.SVGImage_new(path.toNativeUtf8().cast()) {
    var status = _lib.SVGImage_getStatus(_ptr);

    if (status != DartImage_ErrorStatus.STATUS_OK) {
      throw DartImageError.fromCode(status);
    }
  }

  void dispose() {
    _lib.SVGImage_dispose(_ptr);
  }
}

class TextExtents {
  final int width;
  final int height;

  TextExtents(this.width, this.height);
}

class DartImageError extends FFIError {
  static final _codeOffset = FFIError.nextOffset;

  @override
  int get codeOffset => _codeOffset;

  @override
  final String message;
  @override
  final int code;

  DartImageError(this.message, this.code);

  static String _codeToMessage(int code) {
    switch (code) {
      case DartImage_ErrorStatus.STATUS_FILE_NOT_FOUND:
        return 'File not found';
      case DartImage_ErrorStatus.STATUS_FILE_NOT_READABLE:
        return 'File not readable';
      case DartImage_ErrorStatus.STATUS_FILE_NOT_WRITABLE:
        return 'File not writable';
      case DartImage_ErrorStatus.STATUS_CAIRO_UNEXPECTED_ERROR:
        return 'Unexpected Cairo error';
      case DartImage_ErrorStatus.STATUS_RSVG_UNEXPECTED_ERROR:
        return 'Unexpected rsvg error';
      case DartImage_ErrorStatus.STATUS_GLIB_UNEXPECTED_ERROR:
        return 'Unexpected GLib error';
      default:
        return 'Unexpected libdartimage error';
    }
  }

  factory DartImageError.fromCode(int code) =>
      DartImageError(_codeToMessage(code), code);

  @override
  String toString() => 'DartImage error: $message';
}

class RasterImage extends _CDEFObject {
  final Pointer<Void> _ptr;

  double get width => _lib.CairoImage_getWidth(_ptr);

  double get height => _lib.CairoImage_getHeight(_ptr);

  static Pointer<Void> _convertFontToC(Font font) =>
      DartImageLibrary.instance.FontUtils_buildFont(
          font.face.toNativeUtf8().cast(), font.slant, font.weight, font.size);

  static Pointer<Void> _convertColorToC(Color color) => DartImageLibrary
      .instance
      .ColorUtils_buildRGBA(color.red, color.green, color.blue, color.alpha);

  static TextExtents getTextExtents(String text, Font font) {
    var canvas = RasterImage(10000, 10000);

    var _font = _convertFontToC(font);

    var exptr = canvas._lib.CairoImage_getFontExtents(
        canvas._ptr, text.toNativeUtf8().cast(), _font);

    canvas._lib.FontUtils_disposeFont(_font);

    canvas.dispose();

    return TextExtents(
      exptr.ref.width.ceil(),
      exptr.ref.height.ceil(),
    );
  }

  RasterImage(int width, int height, [int pixfmt = PixelFormat.rgb])
      : _ptr = _CDEFObject._libStatic.CairoImage_new(width, height, pixfmt);

  factory RasterImage.fromPNG(String path) {
    var ptr =
        _CDEFObject._libStatic.CairoImage_fromPng(path.toNativeUtf8().cast());

    var status = _CDEFObject._libStatic.CairoImage_getStatus(ptr);

    if (status != DartImage_ErrorStatus.STATUS_OK) {
      throw DartImageError.fromCode(status);
    }

    return RasterImage._fromPtr(ptr);
  }

  RasterImage._fromPtr(this._ptr);

  void drawImage(RasterImage image, double x, double y,
      {double? width, double? height, double? scale}) {
    if (scale != null) {
      _lib.CairoImage_drawImageScaled(_ptr, image._ptr, x, y, scale);
    } else if (width != null && height != null) {
      _lib.CairoImage_drawImageScaledAuto(
          _ptr, image._ptr, x, y, width, height);
    } else {
      _lib.CairoImage_drawImage(_ptr, image._ptr, x, y);
    }
  }

  void drawSVGScaled(
      SVGImage image, double x, double y, double width, double height) {
    _lib.CairoImage_drawSVGScaled(_ptr, image._ptr, x, y, width, height);
  }

  void drawSVG(SVGImage image, double x, double y, double scale) {
    _lib.CairoImage_drawSVG(_ptr, image._ptr, x, y, scale);
  }

  void fill(Color color) {
    var _color = _convertColorToC(color);

    _lib.CairoImage_fillRGB(_ptr, _color);

    _lib.ColorUtils_dispose(_color);
  }

  RasterImage resize(double width, double height) {
    var ptr = _lib.CairoImage_resizeAuto(_ptr, width, height);

    return RasterImage._fromPtr(ptr);
  }

  RasterImage scale(double factor) {
    var ptr = _lib.CairoImage_resize(_ptr, factor);

    return RasterImage._fromPtr(ptr);
  }

  RasterImage clone() {
    var ptr = _lib.CairoImage_clone(_ptr);

    return RasterImage._fromPtr(ptr);
  }

  void saveAsPng(String path) {
    var ret = _lib.CairoImage_saveAsPNG(_ptr, path.toNativeUtf8().cast());

    if(ret != DartImage_ErrorStatus.STATUS_OK) {
      throw DartImageError.fromCode(ret);
    }
  }

  void drawText(String text, Font font, Color color, int x, int y,
      {bool outlined = false}) {
    var _color = _convertColorToC(color);
    var _font = _convertFontToC(font);

    _lib.CairoImage_drawText(
        _ptr, text.toNativeUtf8().cast(), _font, _color, x, y, outlined);
  }

  void dispose() {
    _lib.CairoImage_dispose(_ptr);
  }
}


/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'package:grinder/grinder.dart';
import 'package:logging/logging.dart';

import 'build_native.dart';
import 'ffigen.dart' as ffigen;

void main(List<String> args) {
  Logger.root.onRecord.listen(
        (record) =>
        print("${record.level.name}: [${record.loggerName}] ${record.message}"),
  );

  grind(args);
}

@Task('Generate ffi bindings')
Future<void> ffiGenerate() async {
  var targets = ['dartavconv', 'dartimage'];

  await Future.wait(targets.map((e) => ffigen.generate(e)));
}

@Task('Build native library')
Future<void> buildNative() async {
  var native = await configure('dartimage');
  await build(native);
}
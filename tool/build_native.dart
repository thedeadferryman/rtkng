/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:io';

import 'package:path/path.dart' as p;
import 'package:rtkng/util/sys/command.dart';
import 'package:rtkng/util/sys/tempfile.dart';
import 'package:rtkng/util/sys/which.dart';

String osify(String path) => p.joinAll(p.split(path));

String get _scriptDir => p.dirname(p.fromUri(Platform.script));

final _rootDir =
    osify(p.canonicalize(p.join(_scriptDir, '..')));

String _nativeDir(String name) => osify(p.canonicalize(
      p.join(_scriptDir, '../lib/$name/native'),
    ));

Future<String> get cmake => WhichExecutable([]).resolve('cmake');

const verbosity = Verbosity(stdout: true, stderr: true);

Future<TempDir> configure(String name) async {
  var dir = _nativeDir(name);

  var tempdir = await TempDir.create(prefix: 'grind-native-');

  Directory.current = tempdir.toString();

  var cmd = ShellCommand(await cmake, [
    '-GNinja',
    '-DCMAKE_INSTALL_PREFIX=$_rootDir',
    '-DCMAKE_BUILD_TYPE=Release',
    dir,
  ]);

  var ex = await cmd.execute(verbosity: verbosity);

  await ex.exitCode.take(1).toList().then((value) => value.first);

  return tempdir;
}

Future<TempDir> build(TempDir tempDir) async {
  var cmd = ShellCommand(await cmake, ['--build', '$tempDir', '-t', 'install']);

  var ex = await cmd.execute(verbosity: verbosity);

  await ex.exitCode.take(1).toList().then((value) => value.first);

  return tempDir;
}

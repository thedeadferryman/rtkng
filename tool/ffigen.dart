/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'dart:io';

import 'package:ffigen/ffigen.dart';
import 'package:path/path.dart' as p;
import 'package:yaml/yaml.dart';

final _ffigenConfigDir =
    p.join(p.dirname(p.fromUri(Platform.script)), '../ffigen');

Future<Config> loadConfigFor(String name,
    {String compilerOpts = '', List<String> llvmPath = const []}) async {
  var file = File(p.canonicalize(p.join(_ffigenConfigDir, '$name.yml')));

  if (!await file.exists()) {
    throw ArgumentError.value(name, 'name', 'Config does not exist');
  }

  var data = loadYaml(await file.readAsString(), sourceUrl: file.uri);

  if (data is! YamlMap) {
    throw ArgumentError.value(data, '$name.yml', 'Invalid data');
  }

  var pData = Map.of(data);

  pData['llvm-path'] ??= [];
  pData['llvm-path'] += llvmPath;

  var config = Config.fromYaml(YamlMap.wrap(pData));

  config.addCompilerOpts(compilerOpts, highPriority: true);

  return config;
}

Future<void> generate(String name,
    {String compilerOpts = '', List<String> llvmPath = const []}) async {
  var cfg = await loadConfigFor(name,
      compilerOpts: compilerOpts, llvmPath: llvmPath);

  var library = parse(cfg);

  library.generateFile(File(cfg.output));
}

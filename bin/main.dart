/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import 'package:path/path.dart' as p;
import 'package:rtkng/rtkng.dart';

AppContext get appContext => AppContext(
      resourcesPath: p.join(appProperties.appRoot, 'resources'),
    );

RTKngRunner get runner =>
    RTKngRunner(appContext)..registerApplets(appletFactories);

Future<void> main(List<String> args) => runner.run(args);
